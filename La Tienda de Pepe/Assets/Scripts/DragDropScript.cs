﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Script ajustado a partir de: https://www.youtube.com/watch?v=7XdEVawBpo4 // https://drive.google.com/file/d/0B__1zp7jwQOKeGtoRUJJc1ZMSVE/view

public class DragDropScript : MonoBehaviour
{

    //Initialize Variables


    Vector3 offsetValue;
    Vector3 positionOfScreen;
    LevelManager lm;
    GameManager gameManager;
    GameObject gameobject;
    GameObject target = null;
    RaycastHit2D rayHit;
    Camera cameraUI;

    List<GameObject> pointedProducts;
    List<GameObject> allpointedObjects;

    [SerializeField]
    private GameObject canvasHelp;
    private Canvas canvasHelpComponent;
    public bool isMouseDragging;
    public bool isPointingAButton;

    // Use this for initialization
    void Start()
    {
        lm = FindObjectOfType<LevelManager>();
        gameManager = FindObjectOfType<GameManager>();
        cameraUI = GameObject.Find("Camera_ui").GetComponent<Camera>();
        canvasHelpComponent = canvasHelp.GetComponent<Canvas>();
        pointedProducts = new List<GameObject>();
        allpointedObjects = new List<GameObject>();
        isPointingAButton = false;
    }

    void Update()
    {
        if (canvasHelpComponent.isActiveAndEnabled)
        {
            // Debug.Log("is Active Help Interface");
        }
        else
        {
            // no se pueden mover objetos mientras cliente sale o mientras el menú está activo
            if (lm.customerManager.goIn == false || gameManager.menu.GetComponent<Canvas>().enabled == true)
            {
                isMouseDragging = false;
            }

            //Mouse Button Press Down
            if (Input.GetMouseButtonDown(0))
            {

                if (GameManager.isSomeMenuActive == false)// | GameManager.help == false)
                {
                    gameobject = ReturnClickedObject(); // gameObject es lo que resulte del método...
                    if (gameobject != null)
                    {
                        isMouseDragging = true;
                        //Converting world position to screen position.
                        positionOfScreen = Camera.main.WorldToScreenPoint(gameobject.transform.position);
                        offsetValue = gameobject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, positionOfScreen.z));
                        gameManager.soundManager.PlayDrag();
                    }
                }

            }

            //Mouse Button Up
            if (Input.GetMouseButtonUp(0))
            {
                DetectWhereWasReleased();

                if (target != null && target.tag == "product")
                {
                    target.GetComponent<Product>().isbeingDragging = false;
                }

                isMouseDragging = false;
            }

            //Is mouse Moving
            if (isMouseDragging)
            {
                //tracking mouse position.
                Vector3 currentScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, positionOfScreen.z);

                //converting screen position to world position with offset changes.
                Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + offsetValue;

                //It will update target gameobject's current postion.
                gameobject.transform.position = currentPosition;
            }

            CheckMousePointing();
        }
    }

    public void CheckMousePointing()
    {
        RaycastHit2D[] raycastHits;

        raycastHits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity);

        for (int i = 0; i < raycastHits.Length; i++)
        {

            if (raycastHits[i].collider.gameObject != null)
            {
                allpointedObjects.Add(raycastHits[i].collider.gameObject);
            }

            if (raycastHits[i].collider != null && raycastHits[i].collider.tag == "product")
            {

                raycastHits[i].collider.GetComponent<Product>().isPointed = true;

                if (!pointedProducts.Contains(raycastHits[i].collider.gameObject))
                {
                    pointedProducts.Add(raycastHits[i].collider.gameObject);
                }
            }
        }

        // esto no me funcionaba si usaba un foreach...  parece que el còdigo se sigue ejecutando si usaba foreach... y se veìa afectado
        //al intentar limpiar la lista de todos los objetos seleccionados... por ahì se hablaba tambièn de un lock... para que no avanzarà 
        // hasta terminar la iteraciòn.
        for (int i = 0; i < pointedProducts.Count; i++)
        {
            GameObject gameObject = pointedProducts[i];
            if (!allpointedObjects.Contains(gameObject))
            {
                gameObject.GetComponent<Product>().isPointed = false;
                pointedProducts.Remove(gameObject);
            }
        }

        allpointedObjects.Clear();

        HideMenus();

    }


    public void HideMenus()
    {

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D[] rayshits;

            int layerMask = 1 << 11;

            rayshits = Physics2D.RaycastAll(cameraUI.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity, layerMask);

            // el rayo solamente detecta lo que está en layer 11 (Clickable), por tanto, a menos que toque lo que estè ahí... la lista dará vacío...

            if (rayshits.Length == 0)
            {
                if (GameManager.levelEnd == false && GameManager.help == false)
                {
                    gameManager.CloseMenu();
                }

                if (GameManager.help == true)
                {
                    gameManager.HideHelp();
                }

            }
        }
    }


    //Method to Return Clicked Object
    GameObject ReturnClickedObject()
    {
        target = null;

        rayHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity, ~(LayerMask.GetMask("Store")));


        if (rayHit.collider != null && rayHit.collider.tag == "product")
        {
            target = rayHit.collider.gameObject;
            target.GetComponent<Product>().isbeingDragging = true;
        }

        return target;
    }

    // método para detectar dónde fue liberado el objeto de tal manera que pueda ignorar el collider del counter
    public void DetectWhereWasReleased()
    {
        RaycastHit2D[] hits;
        bool counterWasFound = false;

        hits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity, LayerMask.GetMask("Store"));

        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider != null && hits[i].collider.tag == "counter" && target != null)
            {
                target.GetComponent<BoxCollider2D>().enabled = true;
                i = hits.Length;
                counterWasFound = true;
                gameManager.soundManager.PlayDrop();
            }
        }

        if (counterWasFound == false && target != null)
        {
            target.GetComponent<BoxCollider2D>().enabled = false;

        }
    }

}