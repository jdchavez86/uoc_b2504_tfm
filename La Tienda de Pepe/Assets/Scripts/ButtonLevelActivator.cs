﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ButtonLevelActivator : MonoBehaviour
{

    GameManager gameManager;
    Button button;
    Image image;
    public int level;
    
    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        button = GetComponent<Button>();
        image = GetComponent<Image>();
        Activate();
        button.onClick.AddListener(gameManager.soundManager.PlaySelectLevel);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Activate()
    {
        switch (level)
        {
            case 8:
                button.enabled = true;
                break;
            case 1:
                if (gameManager.saveSystem.level8Points != -99)
                {
                    button.enabled = true;
                    Color tempColor = image.color;
                    tempColor.a = 1f;
                    image.color = tempColor;
                    transform.Find("image_padlock").GetComponentInChildren<Image>().enabled = false;
                    transform.GetComponentInChildren<TextMeshProUGUI>().text = "2";
                }
                else
                {
                    button.enabled = false;
                    Color tempColor = image.color;
                    tempColor.a = .5f;
                    image.color = tempColor;
                    
                }
                break;
                
            case 2:
                if (gameManager.saveSystem.level1Points != -99)
                {
                    button.enabled = true;
                    Color tempColor = image.color;
                    tempColor.a = 1f;
                    image.color = tempColor;
                    transform.Find("image_padlock").GetComponentInChildren<Image>().enabled = false;
                    transform.GetComponentInChildren<TextMeshProUGUI>().text = "3";
                }
                else
                {
                    button.enabled = false;
                    Color tempColor = image.color;
                    tempColor.a = .5f;
                    image.color = tempColor;
                }
                break;
            case 3:
                if (gameManager.saveSystem.level2Points != -99)
                {
                    button.enabled = true;
                    Color tempColor = image.color;
                    tempColor.a = 1f;
                    image.color = tempColor;
                    transform.Find("image_padlock").GetComponentInChildren<Image>().enabled = false;
                    transform.GetComponentInChildren<TextMeshProUGUI>().text = "4";
                }
                else
                {
                    button.enabled = false;
                    Color tempColor = image.color;
                    tempColor.a = .5f;
                    image.color = tempColor;
                }
                break;
            case 4:
                if (gameManager.saveSystem.level3Points != -99)
                {
                    button.enabled = true;
                    Color tempColor = image.color;
                    tempColor.a = 1f;
                    image.color = tempColor;
                    transform.Find("image_padlock").GetComponentInChildren<Image>().enabled = false;
                    transform.GetComponentInChildren<TextMeshProUGUI>().text = "5";
                }
                else
                {
                    button.enabled = false;
                    Color tempColor = image.color;
                    tempColor.a = .5f;
                    image.color = tempColor;
                }
                break;
            case 5:
                if (gameManager.saveSystem.level4Points != -99)
                {
                    button.enabled = true;
                    Color tempColor = image.color;
                    tempColor.a = 1f;
                    image.color = tempColor;
                    transform.Find("image_padlock").GetComponentInChildren<Image>().enabled = false;
                    transform.GetComponentInChildren<TextMeshProUGUI>().text = "6";
                }
                else
                {
                    button.enabled = false;
                    Color tempColor = image.color;
                    tempColor.a = .5f;
                    image.color = tempColor;
                }
                break;
            case 6:
                if (gameManager.saveSystem.level5Points != -99)
                {
                    button.enabled = true;
                    Color tempColor = image.color;
                    tempColor.a = 1f;
                    image.color = tempColor;
                    transform.Find("image_padlock").GetComponentInChildren<Image>().enabled = false;
                    transform.GetComponentInChildren<TextMeshProUGUI>().text = "7";
                }
                else
                {
                    button.enabled = false;
                    Color tempColor = image.color;
                    tempColor.a = .5f;
                    image.color = tempColor;
                }
                break;
            case 7:
                if (gameManager.saveSystem.level6Points != -99)
                {
                    button.enabled = true;
                    Color tempColor = image.color;
                    tempColor.a = 1f;
                    image.color = tempColor;
                    transform.Find("image_padlock").GetComponentInChildren<Image>().enabled = false;
                    transform.GetComponentInChildren<TextMeshProUGUI>().text = "8";
                }
                else
                {
                    button.enabled = false;
                    Color tempColor = image.color;
                    tempColor.a = .5f;
                    image.color = tempColor;
                }
                break;
        }
    }
    
}
