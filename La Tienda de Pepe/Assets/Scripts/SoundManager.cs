﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{

    public AudioSource music;
    public AudioSource sfx;
    public AudioClip openingMusic;
    public AudioClip inGameMusic;
    public AudioClip drag;
    public AudioClip drop;
    public AudioClip key;
    public AudioClip win;
    public AudioClip lose;
    public AudioClip ok;
    public AudioClip wrong;
    public AudioClip beep1;
    public AudioClip beep2;
    public AudioClip buttonUI;
    public AudioClip register;
    public AudioClip mouseOver;
    public AudioClip selectLevel;
    public Button audioButton;
    public bool isPlaying;
    

    // Start is called before the first frame update


    private void Awake()
    {
        music = transform.Find("music").GetComponent<AudioSource>();
        sfx = transform.Find("sfx").GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void Start()
    {
        GameObject[] otherSoundManager = GameObject.FindGameObjectsWithTag("sound_manager");

        if (otherSoundManager.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (SceneManager.GetActiveScene().name == "Main")
        {
            audioButton = GameObject.Find("button_sound").GetComponent<Button>();

            audioButton.onClick.AddListener(ControlAudio);
          
        }

        if (music != null) // esto toca hacerlo porque al volver a la escena main, se crea un nuevo scene manager que es destruido...
        {
            if (music.mute == false)
            {
                isPlaying = true;
            }
            else
            {
                isPlaying = false;
            }
        }

    }

    public void ControlAudio()
    {

        if (isPlaying == true)
        {
            Silence();
            isPlaying = false;
            Color tempColor = audioButton.GetComponent<Image>().color;
            tempColor.a = .5f;
            audioButton.GetComponent<Image>().color = tempColor;
        }
        else
        {
            UnSilence();
            isPlaying = true;
            Color tempColor = audioButton.GetComponent<Image>().color;
            tempColor.a = 1f;
            audioButton.GetComponent<Image>().color = tempColor;
        }
    }

    public void Silence()
    {
        music.mute = true;
       // sfx.mute = true;
    }

    public void UnSilence()
    {
        music.mute = false;
       // sfx.mute = false;
    }

    public void PlayOpeningMusic()
    {
        music.clip = openingMusic;
        music.Play();
    }

    public void PlayInGameMusic()
    {
        music.clip = inGameMusic;
        music.Play();
    }

    public void PlayDrag()
    {
        sfx.PlayOneShot(drag, 1f);
    }

    public void PlayDrop()
    {
        sfx.PlayOneShot(drop, 1f);
    }

    public void PlayKey()
    {
        sfx.PlayOneShot(key, 1f);
    }

    public void PlayWin()
    {
        sfx.PlayOneShot(win, 1f);
    }

    public void PlayLose()
    {
        sfx.PlayOneShot(lose, 1f);
    }

    public void PlayBeep1()
    {
        sfx.PlayOneShot(beep1, 1f);
    }

    public void PlayBeep2()
    {
        sfx.PlayOneShot(beep2, 1f);

    }

    public void PlayUIButton()
    {
        sfx.PlayOneShot(buttonUI, 1f);
    }

    public void PlayOk()
    {
        sfx.PlayOneShot(ok, 1f);
        sfx.PlayOneShot(register, 1f);
    }

    public void PlayWrong()
    {
        sfx.PlayOneShot(wrong, 1f);
    }

    public void PlayMouseOver()
    {
        sfx.PlayOneShot(mouseOver, 1f);
    }

    public void PlaySelectLevel()
    {
        sfx.PlayOneShot(selectLevel, 1f);
    }
}
