﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PriceOutput : MonoBehaviour
{

    public TextMeshProUGUI totalValue;
    private GameManager gameManager;
    [HideInInspector] public string buttonNumber;
    

    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   
    public void AddNewNumber()
    {
        
        totalValue.text = totalValue.text + buttonNumber;

        if (totalValue.text.Length >= 3)
        {
            totalValue.text = totalValue.text.Substring(0, 2);
        }
        
    }

    public void ErraseNumber()
    {
        if (totalValue.text.Length > 0)
        {
            totalValue.text = totalValue.text.Remove(totalValue.text.Length - 1);
        }

        gameManager.soundManager.PlayKey();
    }
}
