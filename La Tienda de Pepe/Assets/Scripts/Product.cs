﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Product : MonoBehaviour
{
    LevelManager lm;
    public Products_SO productSO; //SO por scriptable game object.
    private Image image;
    private Vector3 initialPosition;
    public string productName;
    public int productPrice;
    public int productId;
    private int resolutionX;
    private int resolutionY;
    public bool isChosen = false; // para saber si fue escogido para una transacción
    public bool isReturning;
    public bool isbeingDragging;
    public bool isCollidingWithCounter;
    public bool itsAClientProduct; // si es un producto del cliente, en ejercicio resuelto, ya no retorna a su posición inicial
    public bool isPointed; // indica si el producto está siendo apuntado con el mouse...
    public float speed = 20f;
    private float width;
    private float height;
    
    void OnEnable()
    
    {
        image = GetComponent<Image>();
        lm = FindObjectOfType<LevelManager>();
        switch (productId)
        {
            case 11:

                productName = productSO.fruits.appleClass.fruitName;
                productPrice = productSO.fruits.appleClass.price;
                Sprite productSprite = productSO.fruits.appleClass.appleSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.appleContainer.transform.position;
                
                
                break;

            case 12:

                productName = productSO.fruits.pearClass.fruitName;
                productPrice = productSO.fruits.pearClass.price;
                productSprite = productSO.fruits.pearClass.pearSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.pearContainer.transform.position;
                break;


            case 13:

                productName = productSO.fruits.lemonClass.fruitName;
                productPrice = productSO.fruits.lemonClass.price;
                productSprite = productSO.fruits.lemonClass.lemonSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.lemonContainer.transform.position;
                break;

            case 14:

                productName = productSO.fruits.bananaClass.fruitName;
                productPrice = productSO.fruits.bananaClass.price;
                productSprite = productSO.fruits.bananaClass.bananaSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.bananaContainer.transform.position;
                break;

            case 21:

                productName = productSO.personalProducts.soapClass.productName;
                productPrice = productSO.personalProducts.soapClass.price;
                productSprite = productSO.personalProducts.soapClass.productSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.soapContainer.transform.position;
                break;

            case 22:

                productName = productSO.personalProducts.toothbrushClass.productName;
                productPrice = productSO.personalProducts.toothbrushClass.price;
                productSprite = productSO.personalProducts.toothbrushClass.productSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.toothbrushContainer.transform.position;
                break;

            case 23:

                productName = productSO.personalProducts.toothpasteClass.productName;
                productPrice = productSO.personalProducts.toothpasteClass.price;
                productSprite = productSO.personalProducts.toothpasteClass.productSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.toothpasteContainer.transform.position;
                break;

            case 24:

                productName = productSO.personalProducts.shampooClass.productName;
                productPrice = productSO.personalProducts.shampooClass.price;
                productSprite = productSO.personalProducts.shampooClass.productSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.shampooContainer.transform.position;
                break;

            case 31:

                productName = productSO.stationaryClass.rulerClass.productName;
                productPrice = productSO.stationaryClass.rulerClass.price;
                productSprite = productSO.stationaryClass.rulerClass.productSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.rulerContainer.transform.position;
                break;

            case 32:

                productName = productSO.stationaryClass.pencilClass.productName;
                productPrice = productSO.stationaryClass.pencilClass.price;
                productSprite = productSO.stationaryClass.pencilClass.productSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.pencilContainer.transform.position;
                break;

            case 33:

                productName = productSO.stationaryClass.notebookClass.productName;
                productPrice = productSO.stationaryClass.notebookClass.price;
                productSprite = productSO.stationaryClass.notebookClass.productSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.notebookContainer.transform.position;
                break;

            case 34:

                productName = productSO.stationaryClass.scissorsClass.productName;
                productPrice = productSO.stationaryClass.scissorsClass.price;
                productSprite = productSO.stationaryClass.scissorsClass.productSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.scissorsContainer.transform.position;
                break;

            case 41:

                productName = productSO.clothesClass.jeanClass.productName;
                productPrice = productSO.clothesClass.jeanClass.price;
                productSprite = productSO.clothesClass.jeanClass.productSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.jeanContainer.transform.position;
                break;

            case 42:

                productName = productSO.clothesClass.shirtClass.productName;
                productPrice = productSO.clothesClass.shirtClass.price;
                productSprite = productSO.clothesClass.shirtClass.productSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.shirtContainer.transform.position;
                break;

            case 43:

                productName = productSO.clothesClass.skirtClass.productName;
                productPrice = productSO.clothesClass.skirtClass.price;
                productSprite = productSO.clothesClass.skirtClass.productSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.skirtContainer.transform.position;
                break;

            case 44:

                productName = productSO.clothesClass.shoesClass.productName;
                productPrice = productSO.clothesClass.shoesClass.price;
                productSprite = productSO.clothesClass.shoesClass.productSprite;
                image.sprite = productSprite;
                image.preserveAspect = true;
                initialPosition = lm.itemConstructor.shoesContainer.transform.position;
                break;
        }

    }

    private void Start()
    {
        isReturning = true;
        resolutionX = Screen.width;
        resolutionY = Screen.height;
        this.transform.SetAsFirstSibling();
    }

    // Update is called once per frame
    void Update()
    {

        //if (Vector2.Distance(GetComponent<RectTransform>().anchoredPosition,initialPosition) > .05f && isbeingDragging == false && isReturning == true && itsAClientProduct == false)// && customerManager.goIn == true)
        if (Vector2.Distance(transform.position, initialPosition) > .05f && isbeingDragging == false && isReturning == true && itsAClientProduct == false)
        {
            //GetComponent<RectTransform>().anchoredPosition = Vector3.MoveTowards(GetComponent<RectTransform>().anchoredPosition, initialPosition, Time.deltaTime * speed);
            transform.position = Vector2.MoveTowards(transform.position, initialPosition, Time.deltaTime * speed);
        }


        //if (Vector2.Distance(GetComponent<RectTransform>().anchoredPosition, initialPosition) < .05f) // asì se recupera la colisión después de haberla perdido por dejar el objeto en lugar diferente a counter
        if (Vector2.Distance(transform.position, initialPosition) < .05f)
        {
            GetComponent<BoxCollider2D>().enabled = true;
        }

        if (resolutionX != Screen.width || resolutionY != Screen.height) // Con esto puedo detectar cambios en la resolución.
        {

            resolutionX = Screen.width;
            resolutionY = Screen.height;
            SetNewInitialPosition();
        }

        DetectIfProducIsBeingPointed(); //poner esto en el Drag&Drop...
    }

    public void DetectIfProducIsBeingPointed()
    {
        if (isPointed == true) {

            switch (productId)
            {
                case 11:
                    image.sprite = productSO.fruits.appleClass.appleSpriteAlternative;
                    break;
                case 12:
                    image.sprite = productSO.fruits.pearClass.alternativeSprite;
                    break;
                case 13:
                    image.sprite = productSO.fruits.lemonClass.alternativeSprite;
                    break;
                case 14:
                    image.sprite = productSO.fruits.bananaClass.alternativeSprite;
                    break;
                case 21:
                    image.sprite = productSO.personalProducts.soapClass.alternativeSprite;
                    break;
                case 22:
                    image.sprite = productSO.personalProducts.toothbrushClass.alternativeSprite;
                    break;
                case 23:
                    image.sprite = productSO.personalProducts.toothpasteClass.alternativeSprite;
                    break;
                case 24:
                    image.sprite = productSO.personalProducts.shampooClass.alternativeSprite;
                    break;
                case 31:
                    image.sprite = productSO.stationaryClass.rulerClass.alternativeSprite;
                    break;
                case 32:
                    image.sprite = productSO.stationaryClass.pencilClass.alternativeSprite;
                    break;
                case 33:
                    image.sprite = productSO.stationaryClass.notebookClass.alternativeSprite;
                    break;
                case 34:
                    image.sprite = productSO.stationaryClass.scissorsClass.alternativeSprite;
                    break;
                case 41:
                    image.sprite = productSO.clothesClass.jeanClass.alternativeSprite;
                    break;
                case 42:
                    image.sprite = productSO.clothesClass.shirtClass.alternativeSprite;
                    break;
                case 43:
                    image.sprite = productSO.clothesClass.skirtClass.alternativeSprite;
                    break;
                case 44:
                    image.sprite = productSO.clothesClass.shoesClass.alternativeSprite;
                    break;
            }
        }

        if (isPointed == false)
        {
            switch (productId)
            {
                case 11:
                    image.sprite = productSO.fruits.appleClass.appleSprite;
                    break;
                case 12:
                    image.sprite = productSO.fruits.pearClass.pearSprite;
                    break;
                case 13:
                    image.sprite = productSO.fruits.lemonClass.lemonSprite;
                    break;
                case 14:
                    image.sprite = productSO.fruits.bananaClass.bananaSprite;
                    break;
                case 21:
                    image.sprite = productSO.personalProducts.soapClass.productSprite;
                    break;
                case 22:
                    image.sprite = productSO.personalProducts.toothbrushClass.productSprite;
                    break;
                case 23:
                    image.sprite = productSO.personalProducts.toothpasteClass.productSprite;
                    break;
                case 24:
                    image.sprite = productSO.personalProducts.shampooClass.productSprite;
                    break;
                case 31:
                    image.sprite = productSO.stationaryClass.rulerClass.productSprite;
                    break;
                case 32:
                    image.sprite = productSO.stationaryClass.pencilClass.productSprite;
                    break;
                case 33:
                    image.sprite = productSO.stationaryClass.notebookClass.productSprite;
                    break;
                case 34:
                    image.sprite = productSO.stationaryClass.scissorsClass.productSprite;
                    break;
                case 41:
                    image.sprite = productSO.clothesClass.jeanClass.productSprite;
                    break;
                case 42:
                    image.sprite = productSO.clothesClass.shirtClass.productSprite;
                    break;
                case 43:
                    image.sprite = productSO.clothesClass.skirtClass.productSprite;
                    break;
                case 44:
                    image.sprite = productSO.clothesClass.shoesClass.productSprite;
                    break;
            }

        }
    }

    

    private void SetNewInitialPosition()
    {
        switch (productId)
        {
            case 11:    
                initialPosition = lm.itemConstructor.appleContainer.transform.position;
                break;

            case 12:  
                initialPosition = lm.itemConstructor.pearContainer.transform.position;
                break;

            case 13:
                initialPosition = lm.itemConstructor.lemonContainer.transform.position;
                break;

            case 14:  
                initialPosition = lm.itemConstructor.bananaContainer.transform.position;
                break;

            case 21:  
                initialPosition = lm.itemConstructor.soapContainer.transform.position;
                break;

            case 22:
                initialPosition = lm.itemConstructor.toothbrushContainer.transform.position;
                break;

            case 23:
                initialPosition = lm.itemConstructor.toothpasteContainer.transform.position;
                break;

            case 24:
                initialPosition = lm.itemConstructor.shampooContainer.transform.position;
                break;

            case 31:
                initialPosition = lm.itemConstructor.rulerContainer.transform.position;
                break;

            case 32:
                initialPosition = lm.itemConstructor.pencilContainer.transform.position;
                break;

            case 33:
                initialPosition = lm.itemConstructor.notebookContainer.transform.position;
                break;

            case 34:
                initialPosition = lm.itemConstructor.scissorsContainer.transform.position;
                break;

            case 41:
                initialPosition = lm.itemConstructor.jeanContainer.transform.position;
                break;

            case 42:
               initialPosition = lm.itemConstructor.shirtContainer.transform.position;
                break;

            case 43:
                initialPosition = lm.itemConstructor.skirtContainer.transform.position;
                break;

            case 44:
                initialPosition = lm.itemConstructor.shoesContainer.transform.position;
                break;
        }
    }
    
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "counter")
        {
            isReturning = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "counter")
        {
            isReturning = true;
        }

        if (collision.gameObject.tag == "other")
        {
            isReturning = true;
        }
    } 
}
