﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "LevelPoinsImages", menuName = "Level Point Images", order = 5)]
public class LevelPointsImages_SO : ScriptableObject
{

    public LevelPointsImages levelPointsImages;

}

[System.Serializable]
public class LevelPointsImages
{
    public Sprite points_0;
    public Sprite points_1;
    public Sprite points_2;
    public Sprite points_3;

}
