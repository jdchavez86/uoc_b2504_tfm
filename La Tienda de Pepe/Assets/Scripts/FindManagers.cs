﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FindManagers : MonoBehaviour
{

    public GameObject gameManager;
    private SoundManager soundManager;
    Button menuButton;
    public int buttonDestiny; // 1 para niveles, 2 para salir

    public GameObject buttonNext;


    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("game_manager");
        soundManager = FindObjectOfType<SoundManager>();

        menuButton = GetComponent<Button>();

        if (buttonDestiny == 0)
        {
            menuButton.onClick.AddListener(soundManager.PlayUIButton);
            menuButton.onClick.AddListener(gameManager.GetComponent<GameManager>().CloseMenu);

        }

        if (buttonDestiny == 1)
        {
            menuButton.onClick.AddListener(soundManager.PlayUIButton);
            menuButton.onClick.AddListener(gameManager.GetComponent<GameManager>().ToLevels);


        }

        if (buttonDestiny == 2)
        {
            menuButton.onClick.AddListener(soundManager.PlayUIButton);
            menuButton.onClick.AddListener(gameManager.GetComponent<GameManager>().Quit);

        }

        if (buttonDestiny == 3)
        {
            menuButton.onClick.AddListener(soundManager.PlayUIButton);
            menuButton.onClick.AddListener(soundManager.PlaySelectLevel);
            menuButton.onClick.AddListener(gameManager.GetComponent<GameManager>().ToAnyLevel);


        }

        if (buttonDestiny == 4)
        {
            menuButton.onClick.AddListener(soundManager.PlayUIButton);
            menuButton.onClick.AddListener(gameManager.GetComponent<GameManager>().HideHelp);

        }

        if (buttonDestiny == 5)
        {
            menuButton.onClick.AddListener(soundManager.PlayUIButton);
            menuButton.onClick.AddListener(gameManager.GetComponent<GameManager>().ShowHelp);


        }

        if (buttonDestiny == 6)
        {
            menuButton.onClick.AddListener(soundManager.PlayUIButton);
            menuButton.onClick.AddListener(gameManager.GetComponent<GameManager>().ShowCredits);


        }

        if (buttonDestiny == 7)
        {
            menuButton.onClick.AddListener(soundManager.PlayUIButton);
            menuButton.onClick.AddListener(gameManager.GetComponent<GameManager>().HideCredits);


        }


        //No se busca el GameManager, sino directamente el SoundManager. Si primero busco GM aparece error al traer el SM
        if (buttonDestiny == 8)
        {
            menuButton.onClick.AddListener(soundManager.PlayKey);
        }


        if (buttonDestiny == 9)
        {
            menuButton.onClick.AddListener(soundManager.PlayUIButton);
        }

        if (buttonDestiny == 10)
        {
            menuButton.onClick.AddListener(soundManager.PlayUIButton);
            menuButton.onClick.AddListener(gameManager.GetComponent<GameManager>().ShowLevelMenu);

        }

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ActiveNext()
    {
        buttonNext.GetComponent<Button>().enabled = true;
    }
}
