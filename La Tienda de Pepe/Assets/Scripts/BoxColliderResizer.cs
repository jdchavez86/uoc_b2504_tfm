﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxColliderResizer : MonoBehaviour
{
    
    private int resolutionX;
    private int resolutionY;
    private float width;
    private float height;
    private RectTransform rectTransform;
    private Vector3 [] vCorners;
    
    
    // Start is called before the first frame update
    void Awake()
    {

        resolutionX = Screen.width;
        resolutionY = Screen.height;
        
    }

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        GetCorners();
        ResizeRectTransform();
        ResizeBoxCollider2D();
    }

    
    // Update is called once per frame
    void Update()
    {
        if (resolutionX != Screen.width || resolutionY != Screen.height) // Con esto puedo detectar cambios en la resolución.
        {

            GetCorners();
            ResizeRectTransform();
            ResizeBoxCollider2D();
                
            resolutionX = Screen.width;
            resolutionY = Screen.height;

        }

    }

    void GetCorners()
    {
        vCorners = new Vector3[4];
        rectTransform.GetLocalCorners(vCorners);
        width = Vector3.Distance(vCorners[0],vCorners[3]);
        height = Vector3.Distance(vCorners[0], vCorners[1]);

    }

    void ResizeRectTransform()
    {
        rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
    }

    void ResizeBoxCollider2D()
    {
        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(width, height);
    }

}
