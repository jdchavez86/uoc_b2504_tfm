﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using TMPro;

public class LevelPointsIdentifier : MonoBehaviour
{
    //TextMeshProUGUI levelPoints;
    SaveSystem saveSystem;
    Image image;
    public LevelPointsImages_SO levelPointsImages_SO;
    public GameObject gameManager;
    public int level;
    public int points;

    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("game_manager");
        saveSystem = gameManager.GetComponent<SaveSystem>();
        image= GetComponent<Image>();
        UpdatePoints();

    }

    // Update is called once per frame
    void Update()
    {
      
    }

    public void UpdatePoints()
    {
        switch (level)
        {

            case 1:
                points = saveSystem.level1Points;
                break;

            case 2:
                points = saveSystem.level2Points;
                break;
            case 3:
                points = saveSystem.level3Points;
                break;
            case 4:
                points = saveSystem.level4Points;
                break;
            case 5:
                points = saveSystem.level5Points;
                break;
            case 6:
                points = saveSystem.level6Points;
                break;
            case 7:
                points = saveSystem.level7Points;
                break;
            case 8:
                points = saveSystem.level8Points;
                break;
        }

        switch (points)
        {
            case 1:
                image.sprite = levelPointsImages_SO.levelPointsImages.points_1;
                image.preserveAspect = true;
                break;
            case 2:
                image.sprite = levelPointsImages_SO.levelPointsImages.points_2;
                image.preserveAspect = true;
                break;
            case 3:
                image.sprite = levelPointsImages_SO.levelPointsImages.points_3;
                image.preserveAspect = true;
                break;
            default:
                image.sprite = levelPointsImages_SO.levelPointsImages.points_0;
                image.preserveAspect = true;
                break;
        }
    }
}
