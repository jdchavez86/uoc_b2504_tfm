﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu (fileName = "StarsImages",menuName = "Stars Images", order = 4)]
public class StarsImages_SO : ScriptableObject
{
    public StarImages starImages;
}

[System.Serializable]
public class StarImages
{
    public Sprite star_1;
    public Sprite star_2;
    public Sprite star_3;
    public Sprite star_0;
}
