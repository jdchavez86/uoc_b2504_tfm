﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level_6 : ILevels
{

    // Para cambiar el nivel, se invoca lm.nivelActual y se le asigna el nuevo nivel de acuerdo don las variables asignadas a cada nivel en script LevelManager

    private readonly LevelManager lm;
    private bool payMoney;
    private bool aTypeOfProductWasFound = false;
    private float speed = 10f;
    private float minDistance = .01f;

    public Level_6(LevelManager levelManager)
    {
        lm = levelManager;
    }

    public void UpdateState()
    {
        if (payMoney == true)
        {
            Pay();
        }
    }

    public void SetRoundMount() 
    {
        lm.trials++;
        lm.clientInitialTimeToWait = 30;

        switch (lm.roundCount)
        {
            case 1:
                lm.roundAmount = 20;
                break;

            case 2:
                lm.roundAmount = 50;
                break;
            case 3:
                lm.roundAmount = 80;
                break;
            case 4:
                lm.roundAmount = 99;
                break;
        }
        
        CalculateObjectsQuantity();

    }

    public void CalculateObjectsQuantity() {
        
        lm.activeProducts = GameObject.FindGameObjectsWithTag("product"); // esto no es propiamente de este método... pero me ayuda a no estar creando listas... 

        // se vuelve hacer búsqueda si solamente se encuentra un objeto  
        //si no se encuentran objetos ocurrirá un crash 
        // debe garantizarse suficiente inventario para al menos
        while (lm.productsToShow.Count < 2)
        {
            SearchAndSetObjects();
        }

        //solamente si hay más de un objeto seleccionado se muestran los mismos
        if (lm.productsToShow.Count > 1)
        {
            ShowObjects();
        }
    }

    public void SetObjectsQuantity() {
    }

    public void SearchAndSetObjects() {

        while (aTypeOfProductWasFound == false)
        {
            int category = Random.Range(1, 5);
            int item = Random.Range(1, 5);
            string itemId = category.ToString() + item.ToString();
            string itemName = "";
            int temporalProductPrice = 0;
            int foundObjectsCounter = 0;
            bool priceOk = false;
            bool productAvailability = false;

            switch (itemId)
            {
                case "11":
                    temporalProductPrice = lm.products_SO.fruits.appleClass.price;
                    itemName = lm.products_SO.fruits.appleClass.fruitName;
                    break;
                case "12":
                    temporalProductPrice = lm.products_SO.fruits.pearClass.price;
                    itemName = lm.products_SO.fruits.pearClass.fruitName;
                    break;
                case "13":
                    temporalProductPrice = lm.products_SO.fruits.lemonClass.price;
                    itemName = lm.products_SO.fruits.lemonClass.fruitName;
                    break;
                case "14":
                    temporalProductPrice = lm.products_SO.fruits.bananaClass.price;
                    itemName = lm.products_SO.fruits.bananaClass.fruitName;
                    break;
                case "21":
                    temporalProductPrice = lm.products_SO.personalProducts.soapClass.price;
                    itemName = lm.products_SO.personalProducts.soapClass.productName;
                    break;
                case "22":
                    temporalProductPrice = lm.products_SO.personalProducts.toothbrushClass.price;
                    itemName = lm.products_SO.personalProducts.toothbrushClass.productName;
                    break;
                case "23":
                    temporalProductPrice = lm.products_SO.personalProducts.toothpasteClass.price;
                    itemName = lm.products_SO.personalProducts.toothpasteClass.productName;
                    break;
                case "24":
                    temporalProductPrice = lm.products_SO.personalProducts.shampooClass.price;
                    itemName = lm.products_SO.personalProducts.shampooClass.productName;
                    break;
                case "31":
                    temporalProductPrice = lm.products_SO.stationaryClass.rulerClass.price;
                    itemName = lm.products_SO.stationaryClass.rulerClass.productName;
                    break;
                case "32":
                    temporalProductPrice = lm.products_SO.stationaryClass.pencilClass.price;
                    itemName = lm.products_SO.stationaryClass.pencilClass.productName;
                    break;
                case "33":
                    temporalProductPrice = lm.products_SO.stationaryClass.notebookClass.price;
                    itemName = lm.products_SO.stationaryClass.notebookClass.productName;
                    break;
                case "34":
                    temporalProductPrice = lm.products_SO.stationaryClass.scissorsClass.price;
                    itemName = lm.products_SO.stationaryClass.scissorsClass.productName;
                    break;
                case "41":
                    temporalProductPrice = lm.products_SO.clothesClass.jeanClass.price;
                    itemName = lm.products_SO.clothesClass.jeanClass.productName;
                    break;
                case "42":
                    temporalProductPrice = lm.products_SO.clothesClass.shirtClass.price;
                    itemName = lm.products_SO.clothesClass.shirtClass.productName;
                    break;
                case "43":
                    temporalProductPrice = lm.products_SO.clothesClass.skirtClass.price;
                    itemName = lm.products_SO.clothesClass.skirtClass.productName;
                    break;
                case "44":
                    temporalProductPrice = lm.products_SO.clothesClass.shoesClass.price;
                    itemName = lm.products_SO.clothesClass.shoesClass.productName;
                    break;
            }

            //verifica si el precio del producto es menor que el monto de ronda y si multiplicado por 2 es menor que el monto de ronda
            if (temporalProductPrice < lm.roundAmount && (temporalProductPrice * 2) < lm.roundAmount)
            {
                priceOk = true;
            }

            // verifica la disponibilidad del producto
            if (priceOk == true)
            {
                foreach (GameObject product in lm.activeProducts)
                {
                    Product productInfo = product.GetComponent<Product>();
                    if (productInfo.productId.ToString() == itemId)
                    {
                        foundObjectsCounter++;
                    }
                }

                if (foundObjectsCounter >= 2) // Nunca será mayor a diez porque este es el número máximo de cada tipo de objeto en el inventario.
                {
                    productAvailability = true;
                }
            }

            if (productAvailability == true)
            {
                int productToChooseQuantity = 0;
                int temporalMultiplicationResult = 0;
                bool okFactor = false;

                while (okFactor == false) //encuentra un factor por el cual multiplicar sin superar el monto de la ronda
                {
                    productToChooseQuantity = Random.Range(2, foundObjectsCounter);
                    temporalMultiplicationResult = temporalProductPrice * productToChooseQuantity;
                    if (temporalMultiplicationResult < lm.roundAmount)
                    {
                        okFactor = true;
                    }
                }

                if (okFactor == true) // añade la cantidad de ejemplares del objeto escogido a lista de objetos a mostrar que pide el cliente.
                {
                    lm.sum = temporalMultiplicationResult;

                    for (int i = 0; i < productToChooseQuantity; i++)
                    {
                        lm.productsToShow.Add(itemName);
                    }
                    aTypeOfProductWasFound = true;
                }
            }
        }
    }
    
    public void ShowObjects() { // este método tiene que ejecutarse una vez por trial/ejercicio... no varias veces
        
        foreach (string nameOfProductInList in lm.productsToShow)
        {
            lm.questionMaker.SetActive(true);
            string productName = nameOfProductInList; 
            lm.productInstance = UnityEngine.MonoBehaviour.Instantiate(lm.productSprite, lm.productSprite.transform.position, lm.productSprite.transform.rotation);
            lm.productInstance.transform.SetParent(lm.questionMaker.transform);
            lm.spriteRenderer = lm.productInstance.GetComponent<SpriteRenderer>();
            lm.productsSprites.Add(lm.productInstance);
            
            switch (productName)
            {
                case "apple":
                    lm.spriteRenderer.sprite = lm.products_SO.fruits.appleClass.appleSprite;
                    break;

                case "pear":
                    lm.spriteRenderer.sprite = lm.products_SO.fruits.pearClass.pearSprite;
                    break;

                case "lemon":
                    lm.spriteRenderer.sprite = lm.products_SO.fruits.lemonClass.lemonSprite;
                    break;

                case "banana":
                    lm.spriteRenderer.sprite = lm.products_SO.fruits.bananaClass.bananaSprite;
                    break;

                case "soap":
                    lm.spriteRenderer.sprite = lm.products_SO.personalProducts.soapClass.productSprite;
                    break;

                case "toothbrush":
                    lm.spriteRenderer.sprite = lm.products_SO.personalProducts.toothbrushClass.productSprite;
                    break;

                case "toothpaste":
                    lm.spriteRenderer.sprite = lm.products_SO.personalProducts.toothpasteClass.productSprite;
                    break;

                case "shampoo":
                    lm.spriteRenderer.sprite = lm.products_SO.personalProducts.shampooClass.productSprite;
                    break;
                case "ruler":
                    lm.spriteRenderer.sprite = lm.products_SO.stationaryClass.rulerClass.productSprite;
                    break;
                case "pencil":
                    lm.spriteRenderer.sprite = lm.products_SO.stationaryClass.pencilClass.productSprite;
                    break;
                case "notebook":
                    lm.spriteRenderer.sprite = lm.products_SO.stationaryClass.notebookClass.productSprite;
                    break;
                case "scissors":
                    lm.spriteRenderer.sprite = lm.products_SO.stationaryClass.scissorsClass.productSprite;
                    break;
                case "jean":
                    lm.spriteRenderer.sprite = lm.products_SO.clothesClass.jeanClass.productSprite;
                    break;
                case "shirt":
                    lm.spriteRenderer.sprite = lm.products_SO.clothesClass.shirtClass.productSprite;
                    break;
                case "skirt":
                    lm.spriteRenderer.sprite = lm.products_SO.clothesClass.skirtClass.productSprite;
                    break;
                case "shoes":
                    lm.spriteRenderer.sprite = lm.products_SO.clothesClass.shoesClass.productSprite;
                    break;
            }
        }
    }

    public void SetAndShowMoney() {
    }

    //script que da mucha vuelta... pero es que el botón de confirmación debe ir al script principal levelmanager, no al del nivel... 
    //pues necesitaré levelmanager para controlar varios niveles
    public void CheckObjectAnwser() 
    {
        bool correctItems = lm.objectDetector.CheckObjects();

        if (lm.objectDetector.selectedProducts.Count == 0)
        {
            lm.clientText.text = "¡No está mi pedido!";
            lm.Invoke("ShowObjectsQuestion", 2f);
            lm.questionMaker.SetActive(false);
            lm.confirmButton.enabled = false;
        }

        if (correctItems == false && lm.objectDetector.selectedProducts.Count == lm.productsToShow.Count)
        {
            lm.clientText.text = "No está bien mi pedido";
            lm.questionMaker.SetActive(false);
            lm.Invoke("ShowObjectsQuestion", 2f);
            lm.confirmButton.enabled = false;
        }

        if (correctItems == false && lm.objectDetector.selectedProducts.Count < lm.productsToShow.Count && lm.objectDetector.selectedProducts.Count != 0)
        {
            lm.clientText.text = "Faltan productos";
            lm.questionMaker.SetActive(false);
            lm.Invoke("ShowObjectsQuestion", 2f);
            lm.confirmButton.enabled = false;
        }

        if (lm.objectDetector.selectedProducts.Count > lm.productsToShow.Count)
        {
            lm.clientText.text = "Sobran productos";
            lm.questionMaker.SetActive(false);
            lm.Invoke("ShowObjectsQuestion", 2f);
            lm.confirmButton.enabled = false;
        }

        if (correctItems == true && lm.objectDetector.selectedProducts.Count == lm.productsToShow.Count)
        {
            lm.clientText.text = "¿Cuánto debo?";
            lm.questionMaker.SetActive(false);
            lm.confirmButton.enabled = false;
        }
    }

    public void ShowQuestion() {
    }

    public void CheckInputAnwser()
    {
        lm.confirmButton.enabled = false;
        System.Int32.TryParse(lm.priceOutput.text, out int numValue);
        int priceOutput = numValue;

        if (priceOutput == lm.sum) // si la suma es correcta
        {
            GiveFeedback(1);
        }

        if (priceOutput > lm.sum) // si la suma es mayor
        {
            GiveFeedback(2);
        }

        if (priceOutput < lm.sum) // si la suma es menor... es equivocación también, pero no se castiga... como el cobrar más... se supone cliente
        {
            GiveFeedback(3);
        }
    }

    //entran en x los casos para las condiciones if
    public void GiveFeedback(int x) {

        if (x == 1) { // respuesta correcta
            lm.clientText.text = "¡Muchas gracias!";
            lm.questionMaker.SetActive(false);
            ResolveTrial(1);
            lm.Invoke("CleanText", 2f); // de una vez se borra el priceinput.
            lm.clientIsWaiting = false;
        }

        if (x == 2) // mayor precio que solicitado por el cliente
        {
            lm.clientText.text = "Estás cobrando más";
            lm.questionMaker.SetActive(false);
            ResolveTrial(2);
            lm.Invoke("CleanText", 2f); // de una vez se borra el priceinput.
            lm.clientIsWaiting = false;
        }

        if (x == 3) // menor precio que el solicitado por el cliente // vuelve a mostrar la pregunta.
        {
            if (lm.clientIsWaiting == true)
            {
                lm.clientText.text = "Estás cobrando menos";
                lm.questionMaker.SetActive(false);
                lm.Invoke("ShowObjectsQuestion", 2f);
            }
            else
            {
                lm.clientText.text = "Tengo que irme";
                lm.questionMaker.SetActive(false);
                ResolveTrial(2);
                lm.Invoke("CleanText", 2f); // de una vez se borra el priceinput
            }

            if (lm.pointsToSubtract < 3)
            {
                lm.pointsToSubtract++;
            }
        }
    }

    public void ResolveTrial(int x) {

        if (x == 1) // en caso de respuesta correcta
        {
            lm.okTrialsByRound++;
            
            if (lm.okTrialsByRound == 2)
            {
                lm.okTrialsByRound = 0;
                lm.okRounds++;
                lm.roundCount++;
                CheckLevelSuccess();
            }
            lm.barSuccess.fillAmount = lm.barSuccess.fillAmount + .125f;
            lm.objectDetector.completedList = true;
            lm.objectDetector.giveProducts = true;
            SetClientMoney();
        }

        if (x == 2) // en caso de respuesta incorrecta por exceso
        {
            lm.numberOfFails++;
            CheckLevelSuccess();
            lm.objectDetector.completedList = true;
            lm.objectDetector.returnProducts = true;
            lm.barFail.fillAmount = lm.barFail.fillAmount + .2f;
        }
        lm.customerManager.GetOut();

    }

    public void SetClientMoney()
    {
        GameObject newMoney;
        int[] moneyValues = new int[] {1, 2, 5, 10, 20, 50};
        int changeSum = 0; // sumatoria del cambio
        int indexModifier = moneyValues.Length;
        int tempMoney;

        for (int i = moneyValues.Length; i > 0; i--) // la condición es mientras i sea mayor a 0...
        {
            tempMoney = moneyValues[indexModifier -1];
            
            if (tempMoney > lm.sum)
            {
                indexModifier--;
            }
            
            if (tempMoney <= lm.sum)
            {
                while (true)
                {
                    if ((changeSum + tempMoney) > lm.sum)
                    {
                        break;
                    }

                    changeSum += tempMoney;

                    switch (tempMoney)
                    {
                        case 50:
                            newMoney = UnityEngine.MonoBehaviour.Instantiate(lm.cash_SO.billClass.fiftyBillClass.money, lm.billsToStoreSpawner.position, lm.billsToStoreSpawner.rotation);
                            newMoney.transform.SetParent(lm.billsToStoreSpawner.transform, false);
                            lm.moneyToStore.Add(newMoney);
                            break;

                        case 20:
                            newMoney = UnityEngine.MonoBehaviour.Instantiate(lm.cash_SO.billClass.twentyBillClass.money, lm.billsToStoreSpawner.position, lm.billsToStoreSpawner.rotation);
                            newMoney.transform.SetParent(lm.billsToStoreSpawner.transform, false);
                            lm.moneyToStore.Add(newMoney);
                            break;

                        case 10:
                            newMoney = UnityEngine.MonoBehaviour.Instantiate(lm.cash_SO.billClass.tenBillClass.money, lm.billsToStoreSpawner.position, lm.billsToStoreSpawner.rotation);
                            newMoney.transform.SetParent(lm.billsToStoreSpawner.transform, false);
                            lm.moneyToStore.Add(newMoney);
                            break;

                        case 5:
                            newMoney = UnityEngine.MonoBehaviour.Instantiate(lm.cash_SO.coinClass.fiveCoinClass.money, lm.coinsToStoreSpawner.position, lm.coinsToStoreSpawner.rotation);
                            newMoney.transform.SetParent(lm.coinsToStoreSpawner.transform, false);
                            lm.moneyToStore.Add(newMoney);
                            break;

                        case 2:
                            newMoney = UnityEngine.MonoBehaviour.Instantiate(lm.cash_SO.coinClass.twoCoinClass.money, lm.coinsToStoreSpawner.position, lm.coinsToStoreSpawner.rotation);
                            newMoney.transform.SetParent(lm.coinsToStoreSpawner.transform, false);
                            lm.moneyToStore.Add(newMoney);
                            break;

                        case 1:
                            newMoney = UnityEngine.MonoBehaviour.Instantiate(lm.cash_SO.coinClass.oneCoinclass.money, lm.coinsToStoreSpawner.position, lm.coinsToStoreSpawner.rotation);
                            newMoney.transform.SetParent(lm.coinsToStoreSpawner.transform, false);
                            lm.moneyToStore.Add(newMoney);
                            break;
                    }
                }
                indexModifier--;
            }

            if (changeSum == lm.sum)
            {
                i = 0;
            }
        }
        payMoney = true;
    }

    public void Pay()
    {
        foreach (GameObject money in lm.moneyToStore)
        {
            int moneyValue = money.GetComponent<Money>().value;
            switch (moneyValue)
            {
                case 50:
                    if (Vector2.Distance(money.transform.position, lm.itemConstructor.fiftyBillContainer.transform.position) > minDistance)
                    {
                        money.transform.position = Vector2.MoveTowards(money.transform.position, lm.itemConstructor.fiftyBillContainer.transform.position, Time.deltaTime * speed);
                    }
                    break;

                case 20:
                    
                    if (Vector2.Distance(money.transform.position, lm.itemConstructor.twentyBillContainer.transform.position) > minDistance)
                    {
                        money.transform.position = Vector2.MoveTowards(money.transform.position, lm.itemConstructor.twentyBillContainer.transform.position, Time.deltaTime * speed);
                    }
                    break;

                case 10:
                   
                    if (Vector2.Distance(money.transform.position, lm.itemConstructor.tenBillContainer.transform.position) > minDistance)
                    {
                        money.transform.position = Vector2.MoveTowards(money.transform.position, lm.itemConstructor.tenBillContainer.transform.position, Time.deltaTime * speed);
                    }
                    break;

                case 5:
                    
                    if (Vector2.Distance(money.transform.position, lm.itemConstructor.fiveCoinContainer.transform.position) > minDistance)
                    {
                        money.transform.position = Vector2.MoveTowards(money.transform.position, lm.itemConstructor.fiveCoinContainer.transform.position, Time.deltaTime * speed);
                    }
                    break;

                case 2:
                    if (Vector2.Distance(money.transform.position, lm.itemConstructor.twoCoinContainer.transform.position) > minDistance)
                    {
                        money.transform.position = Vector2.MoveTowards(money.transform.position, lm.itemConstructor.twoCoinContainer.transform.position, Time.deltaTime * speed);
                    }
                    break;

                case 1:
                    if (Vector2.Distance(money.transform.position, lm.itemConstructor.oneCoinContainer.transform.position) > minDistance)
                    {
                        money.transform.position = Vector2.MoveTowards(money.transform.position, lm.itemConstructor.oneCoinContainer.transform.position, Time.deltaTime * speed);
                    }
                    break;
            }
        }
    }

    //Método que se activa desde Customer Manager cuando sale el cliente
    public void RestartTrial()
    {
        aTypeOfProductWasFound = false;
        payMoney = false;
        lm.sum = 0;
        lm.productsToShow.Clear();
        lm.objectDetector.productsOfCustomer.Clear();
        lm.moneyToStore.Clear();
        lm.objectDetector.completedList = false;
        lm.objectDetector.giveProducts = false;
        lm.objectDetector.returnProducts = false;
        lm.confirmButton.enabled = true;
        lm.customerManager.GetIn();
        lm.activeProducts = GameObject.FindGameObjectsWithTag("product");
        foreach (GameObject product in lm.activeProducts)
        {
            Product productScript = product.GetComponent<Product>();
            productScript.isChosen = false;
        }
        foreach (GameObject productSprite in lm.productsSprites)
        {
            UnityEngine.MonoBehaviour.Destroy(productSprite);

        }
        lm.productsSprites.Clear();
    }

    public void CheckLevelSuccess()
    {
        if (lm.okRounds >= 4)
        {
            //nivel ganado
            lm.resultText.text = "¡Ganaste!";
            lm.gameManager.menu.GetComponent<Canvas>().enabled = true;
            GiveStars();
            lm.win = true;
            lm.ShowPoints();
        }

        if (lm.numberOfFails >= lm.maxFails)
        {
            //nivel perdido
            lm.resultText.text = "¡Perdiste!";
            lm.gameManager.menu.GetComponent<Canvas>().enabled = true;
            lm.levelStars = -99;
            lm.win = false;
            lm.ShowPoints();
        }
    }
    public void ChangeLevel()
    {
        
    }

    public void GiveStars() {

        switch (lm.numberOfFails)
        {
            case 0:
                lm.levelStars = 3 - lm.pointsToSubtract;
                break;
            case 1:
                lm.levelStars = 2 - lm.pointsToSubtract;
                break;
            case 2:
                lm.levelStars = 1 - lm.pointsToSubtract;
                break;
            case 3:
                lm.levelStars = 0;
                break;
            case 4:
                lm.levelStars = 0;
                break;
        }

        Save();
    }

    public void Save()
    {
        lm.gameManager.saveSystem.SaveDataByLevel(lm.levelName, lm.levelStars);
    }

    public void OnTriggerEnter(Collider2D col)
    {
        
    }

    public void OnTriggerExit(Collider2D col)
    {

    }
}
