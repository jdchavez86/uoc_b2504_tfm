﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelItemMonitor : MonoBehaviour
{
    public ItemConstructor itemConstructor;
    public Products_SO productSO;
    public Cash_SO cashSO;
   
    //Fruits

    public int applePrice;
    public int appleQuantity;
    
    public int pearPrice;
    public int pearQuantity;
    
    public int bananaPrice;
    public int bananaQuantity;
    
    public int lemonPrice;
    public int lemonQuantity;

    //money

    public int oneCoinQuantity;
    public int oneCoinValue;

    public int twoCoinQuantity;
    public int twoCoinValue;

    public int fiveCoinQuantity;
    public int fiveCoinValue;

    public int tenBillQuantity;
    public int tenBillValue;

    public int twentyBillQuantity;
    public int twentyBillValue;

    public int fiftyBillQuantity;
    public int fiftyBillValue;

    public int totalMoney;


    // Start is called before the first frame update

    void Start()
    {

        

}

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReloadItemData ()
    {
        //Inventory

        // Fruits

        if (itemConstructor.appleContainer.gameObject.activeInHierarchy == true)
        {
            appleQuantity = productSO.fruits.appleClass.amount;
            applePrice = productSO.fruits.appleClass.price;
        }
        
        if (itemConstructor.pearContainer.gameObject.activeInHierarchy == true)
        {
            pearQuantity = productSO.fruits.pearClass.amount;
            pearPrice = productSO.fruits.pearClass.price;
        }

        if (itemConstructor.bananaContainer.gameObject.activeInHierarchy == true)
        {
            bananaQuantity = productSO.fruits.bananaClass.amount;
            bananaPrice = productSO.fruits.bananaClass.price;
        }

        if (itemConstructor.lemonContainer.gameObject.activeInHierarchy == true) {
            lemonQuantity = productSO.fruits.lemonClass.amount;
            lemonPrice = productSO.fruits.lemonClass.price;
        }


        // Money


        if (itemConstructor.oneCoinContainer.gameObject.activeInHierarchy == true)
        {
            oneCoinQuantity = cashSO.coinClass.oneCoinclass.amount;
            oneCoinValue = cashSO.coinClass.oneCoinclass.price;
        }


        if (itemConstructor.twoCoinContainer.gameObject.activeInHierarchy == true)
        {
            twoCoinQuantity = cashSO.coinClass.twoCoinClass.amount;
            twoCoinValue = cashSO.coinClass.twoCoinClass.price;
        }


        if (itemConstructor.fiveCoinContainer.gameObject.activeInHierarchy == true)
        {
            fiveCoinQuantity = cashSO.coinClass.fiveCoinClass.amount;
            fiveCoinValue = cashSO.coinClass.fiveCoinClass.price;
        }
            

        if (itemConstructor.tenBillContainer.gameObject.activeInHierarchy == true)
        {
            tenBillQuantity = cashSO.billClass.tenBillClass.amount;
            tenBillQuantity = cashSO.billClass.tenBillClass.price;
        }
            

        if (itemConstructor.twentyBillContainer.gameObject.activeInHierarchy == true)
        {
            twentyBillQuantity = cashSO.billClass.twentyBillClass.amount;
            twentyBillQuantity = cashSO.billClass.twentyBillClass.price;
        }

        if (itemConstructor.fiftyBillContainer.gameObject.activeInHierarchy == true)
        {
            fiftyBillQuantity = cashSO.billClass.fiftyBillClass.amount;
            fiftyBillQuantity = cashSO.billClass.fiftyBillClass.price;
        }
            
        totalMoney = oneCoinQuantity * oneCoinValue + twoCoinQuantity * twoCoinValue + fiveCoinQuantity * fiveCoinValue + tenBillQuantity * tenBillValue + twentyBillQuantity * twentyBillValue + fiftyBillQuantity * fiftyBillValue;
    }
}
