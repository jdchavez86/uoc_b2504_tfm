﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveMenuDetector : MonoBehaviour
{
    // Start is called before the first frame update

    private BoxCollider2D boxCollider2D;
    //La siguiente variable 1 es collider de menú principal de nivel y 2 es collider menú de ayuda. 
    //Lo necesito para saber qué box collider activar y desactivar de los menus para que sea detectados correctamente
    // al hacer clic por fuera de los botones y cerrar el que se necesita
    public int menuCollider;

    void Start()
    {
        boxCollider2D = GetComponent<BoxCollider2D>();
        boxCollider2D.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (menuCollider == 2)
        {

            if (GameManager.help == true)
            {
                boxCollider2D.enabled = true;
            }
            else
            {
                boxCollider2D.enabled = false;
            }
        }

        if (menuCollider == 1)
        {

            if (GameManager.help == true)
            {
                boxCollider2D.enabled = false;
            }
            else
            {
                boxCollider2D.enabled = true;
            }
        }

    }
}
