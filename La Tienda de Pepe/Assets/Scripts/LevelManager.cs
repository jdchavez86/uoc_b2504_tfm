﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class LevelManager : MonoBehaviour
{

    public int levelName;
    public int okTrialsByRound; //trials correctos por ronda... se supone que deben contestarse bien dos ejercicios por ronda.
    public int okRounds; // cantidad de rondas correctas.
    public int numberOfFails; // múmero de errores cometidos
    public int maxFails = 5; // número máximo de intentos para superar el nivel. // con máximo 4 fallas se pueden hasta 12 intentos para superar nivel // se necesitan pararejas para superar nivel
    public int roundCount = 1; // en qué ronda se encuentra
    public int roundAmount; // valor del monto de la ronda
    public int objectQuantity; // cantidad de objetos que serán mostrados en el ejercicio.
    public int objectsCounter; //cantidad de objetos ya escogidos
    public int sum; // sumatoría para evaluar si se cumple el monto de la transacción.
    public int trials; // número de intentos de nivel
    public int levelStars;
    public int clientInitialTimeToWait; // tiempo que inicialmente tiene el cliente para esperar
    public int clientWaitingTime; // la cuenta regresiva del cliente esperando
    public int pointsToSubtract;
    public bool clientIsWaiting; // dice si el cliente está esperando, si es así, el tiempo de espera se va reduciendo.
    public bool win; // sirve para poner las imágenes de ganado o perdido
    private int prevFails;
    private int prevOks;
    private bool changeInFails; // para detectar si hubo cambios y reproducir el sonido respectivo.
    private bool changeInOkTrial; // para detectar si hubo cambios y reproducir el sonido respectivo.
    private bool enableButton;
    private float elapsedTimeToEnableButton;
    private float waitTimeToEnableButton = 2f;
    private float elapsedTimeToCountDown;

    public GameObject gameName;
    public GameObject banners;
    public GameObject questionMaker;
    public GameObject productSprite;
    public GameObject counter;
    public GameObject nextLevelButton;
    public GameObject restartLevelButton;
    public GameObject substract;
    public GameManager gameManager;
    public RectTransform coinsToStoreSpawner;
    public RectTransform billsToStoreSpawner;

    public Button confirmButton;
    public Image barFail;
    public Image barSuccess;
    public Image points;
    public ParticleSystem winParticles;

    public TextMeshProUGUI clientText;
    public TextMeshProUGUI priceOutput;
    public TextMeshProUGUI resultText;
    public TextMeshProUGUI waitingTimeText;
    public TextMeshProUGUI levelTitle;

    public Cash_SO cash_SO;
    public Products_SO products_SO;
    public StarsImages_SO starsImages_SO;
    public ObjectDetector objectDetector;
    public ItemConstructor itemConstructor;
    public CustomerManager customerManager;

    [HideInInspector] public SpriteRenderer spriteRenderer; // componente que mostrará el sprite del producto que pide el cliente
    [HideInInspector] public GameObject productInstance; // objeto que será creado para mostrar sprite de productos pedidos por cliente.
    [HideInInspector] public GameObject[] activeProducts; // estos son los productos que estarán activos en inventario.
    [HideInInspector] public List<string> productsToShow; //estos son los productos escogidos para mostrar en pregunta
    [HideInInspector] public List<GameObject> moneyToStore = new List<GameObject>();
    [HideInInspector] public List<GameObject> moneyToClient = new List<GameObject>();
    [HideInInspector] public List<GameObject> productsSprites = new List<GameObject>();
    [HideInInspector] public List<Vector2> moneyPositions = new List<Vector2>(); // posiciones del dinero cuando se pone en mostrador para que quede organizado

    public ILevels actualLevel; // aquí se va ir cargando cada nivel.
    public Level_0 level0;
    public Level_1 level1;
    public Level_2 level2;
    public Level_3 level3;
    public Level_4 level4;
    public Level_5 level5;
    public Level_5b level5b;
    public Level_6 level6;
    public Level_7a level7a;
    public Level_7b level7b;
    public Level_7c1 level7c1;
    public Level_7c2 level7c2;
    public Level_7d level7d;

    // desde este script se le puedan dar órdenes a los scripts hijos. Para esto se llama el estado actual

    private void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("game_manager").GetComponent<GameManager>();
        levelName = GameManager.choosedLevel; // como es una variable estática, hay que referirse a la clase y no a la instancia del objeto... Wao!
        //levelName = 8;
    }

    void Start()
    {
        GameManager.levelEnd = false;
        Canvas levelMenu = GameObject.FindGameObjectWithTag("menu").GetComponent<Canvas>();
        levelMenu.enabled = false;
        points.enabled = false;
        changeInFails = false;
        changeInOkTrial = false;
        prevFails = 0;
        prevOks = 0;

        if (levelName != 8)
        {
            int levelTitleId = levelName + 1;
            levelTitle.text = "Nivel " + levelTitleId.ToString();
        }
        else
        {
            levelTitle.text = "Nivel 1";
        }

        level0 = new Level_0(this);
        level1 = new Level_1(this); // This es este objeto... este script... a saber, el script de level manager...
        level2 = new Level_2(this);
        level3 = new Level_3(this);
        level4 = new Level_4(this);
        level5 = new Level_5(this);
        level5b = new Level_5b(this);
        level6 = new Level_6(this);
        level7a = new Level_7a(this);
        level7b = new Level_7b(this);
        level7c1 = new Level_7c1(this);
        level7c2 = new Level_7c2(this);
        level7d = new Level_7d(this);

        switch (levelName)
        {
            case 8:
                actualLevel = level0;
                clientInitialTimeToWait = 60;
                customerManager.Invoke("GetIn", 1f);
                if (gameManager.saveSystem.level8Points == -99) //solo muestra el tutorial una vez al inicio del nivel si no se ha ganado el nivel 1
                {
                    gameManager.ShowHelp();
                }
                break;
            case 1:
                clientInitialTimeToWait = 30;
                actualLevel = level1;
                customerManager.Invoke("GetIn", 1f);
                break;

            case 2:
                clientInitialTimeToWait = 30;
                actualLevel = level2;
                customerManager.Invoke("GetIn", 1f);
                break;

            case 3:
                clientInitialTimeToWait = 30;
                actualLevel = level3;
                customerManager.Invoke("GetIn", 1f);
                break;

            case 4:
                clientInitialTimeToWait = 45;
                actualLevel = level4;
                customerManager.Invoke("GetIn", 1f);
                break;

            case 5:
                clientInitialTimeToWait = 30;
                actualLevel = level5;
                customerManager.Invoke("GetIn", 1f);
                break;
            case 6:
                clientInitialTimeToWait = 30;
                actualLevel = level6;
                customerManager.Invoke("GetIn", 1f);
                break;
            case 7:
                clientInitialTimeToWait = 30;
                actualLevel = level7a;
                customerManager.Invoke("GetIn", 1f);
                break;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        actualLevel.UpdateState();

        // Lo siguiente es para reactivar el botón de confirmar, para evitar que se presione muchas veces seguidas
        elapsedTimeToEnableButton += Time.deltaTime;

        if (customerManager.goIn == true)
        {
            if (elapsedTimeToEnableButton > waitTimeToEnableButton)
            {
                enableButton = true;
                elapsedTimeToEnableButton = 0f;

                if (enableButton == true)
                {
                    confirmButton.enabled = true;
                    enableButton = false;
                }
            }
        }
        else // asì se evita que se pueda hacer clic en Ok mientras el cliente esta saliendo...
        {
            confirmButton.enabled = false;
        }
        

        elapsedTimeToCountDown += Time.deltaTime;

        if (clientIsWaiting == true && gameManager.menu.GetComponent<Canvas>().enabled == false && GameManager.help == false) // si el canvas general o de ayuda está activo, el tiempo para
        {
            if (elapsedTimeToCountDown > 1)
            {
                clientWaitingTime--;
                elapsedTimeToCountDown = 0;

                if (clientWaitingTime == 3 || clientWaitingTime == 2 || clientWaitingTime == 1)
                {

                    if (gameManager.soundManager.sfx.isPlaying == false)
                    {
                        gameManager.soundManager.PlayBeep1();
                    }

                }
            }

            if (clientWaitingTime == 0)
            {
                clientIsWaiting = false;
                Confirm();
                confirmButton.enabled = false; // evita que se puedan enviar más respuestas

                if (gameManager.soundManager.sfx.isPlaying == false)
                {
                    gameManager.soundManager.PlayBeep2();
                }
                
            }
            
            waitingTimeText.text = "" + clientWaitingTime;
        }

        if (prevOks != okTrialsByRound ) // para reproducir sonido ok al realizar bien un intento y pone boca feliz. Se hace aquí para no cambiar el valor desde cada script de nivel.
        {
            prevOks = okTrialsByRound;
            changeInOkTrial = true;

            if (changeInOkTrial == true)
            {
                changeInOkTrial = false;
                gameManager.soundManager.PlayOk();
                customerManager.mouth.sprite = customerManager.happyMouth;
            }
        }

        if (prevFails != numberOfFails) // para reproducir sonido de equivocaciòn al realizar mal un intento y pone boca triste. Se hace aquí para no cambiar el valor desde cada script de nivel.
        {
            prevFails= numberOfFails;
            changeInFails = true;

            if (changeInFails == true)
            {
                changeInFails = false;
                gameManager.soundManager.PlayWrong();
                customerManager.mouth.sprite = customerManager.sadMouth;
            }
        }

        if (GameManager.levelEnd == true && win == true && GameManager.help == true)
        {
            winParticles.Stop();
        }

        if (GameManager.levelEnd == true && win == true && GameManager.help == false && winParticles.isPlaying == false)
        {
            winParticles.Play();
        }

    }

    public void Confirm()
    {
        if  (clientIsWaiting == true) // las siguientes condiciones solamente se evalúan si el cliente está esperando
        {
            //cuando no se ha ingresado un pedido.
            // no es necesario condiciòn que se aplique cuando no hay producto seleccionado y se quiere poner precio-
            // simplemente no se puede escribir precio si no hay producto
            if (objectDetector.selectedProducts.Count == 0)
            {
                CheckObjectsInput();
            }

            //cuando se ha ingresado un pedido y no un precio
            if (objectDetector.selectedProducts.Count > 0 && priceOutput.text == "")
            {
                CheckObjectsInput();
            }

            //cuando se ingresa un precio y el pedido no está bien
            if (objectDetector.selectedProducts.Count > 0 && objectDetector.CheckObjects() == false && priceOutput.text != "")
            {
                CheckObjectsInput();
            }

            // cuando se ponen los objetos correctos pero no un precio
            if (objectDetector.CheckObjects() == true && priceOutput.text == "")
            {
                CheckObjectsInput();
            }
        }

        if (clientIsWaiting == false && customerManager.isInside == true) // aquí se llega cuando se acabó el tiempo y el cliente se va
        {
            CheckPriceInput();
        }
      
        
        // cuando se han ingresado los productos correctos y precio no está vacío y el cliente está esperando
        if (objectDetector.CheckObjects() == true && priceOutput.text != "" && clientIsWaiting == true) 
        {
            CheckPriceInput();
        }

    }

    public void CheckObjectsInput()
    {
        actualLevel.CheckObjectAnwser();
    }

    public void CheckPriceInput()
    {
        actualLevel.CheckInputAnwser();
    }

    public void ShowObjectsQuestion() // Mètodo para mostrar de nuevo los productos que pide el cliente.
    {
        if (clientIsWaiting == true) // este if evita que se muestre de nuevo el estímulo llamado por el invoke cuando por ejemplo es respuesta a draw que se brinda a últimos segundo
        {
            questionMaker.SetActive(true);
            clientText.text = "";
            confirmButton.enabled = true;
        }
    }

    public void CleanText()
    {
        clientText.text = "";
        priceOutput.text ="";
    }

    public void ShowPoints()
    {
        GameManager.levelEnd = true;
        resultText.text = ""; // Ya no se necesita el texto porque serà reemplazado por imagen.
        gameName.SetActive(false);
        Image bannerImageComponent = banners.GetComponent<Image>();
        Banners bannerScript = banners.GetComponent<Banners>();
        GameObject eventsystem = GameObject.Find("EventSystem");

        if (win == true)
        {
            bannerImageComponent.sprite = bannerScript.win;
            bannerImageComponent.preserveAspect = true;
            gameManager.soundManager.PlayWin();
            eventsystem.GetComponent<EventSystem>().SetSelectedGameObject(nextLevelButton);
            winParticles.Play();
        }
        else
        {
            bannerImageComponent.sprite = bannerScript.lose;
            bannerImageComponent.preserveAspect = true;
            gameManager.soundManager.PlayLose();
            eventsystem.GetComponent<EventSystem>().SetSelectedGameObject(restartLevelButton);
            Debug.Log(eventsystem.GetComponent<EventSystem>().currentSelectedGameObject);
        }

        points.enabled = true;
        switch (levelStars)
        {
            case 1:
                points.sprite = starsImages_SO.starImages.star_1;
                points.preserveAspect = true;
               
                break;
            case 2:
                points.sprite = starsImages_SO.starImages.star_2;
                points.preserveAspect = true;
                bannerImageComponent.sprite = bannerScript.win;
                bannerImageComponent.preserveAspect = true;
                break;
            case 3:
                points.sprite = starsImages_SO.starImages.star_3;
                points.preserveAspect = true;
                bannerImageComponent.sprite = bannerScript.win;
                bannerImageComponent.preserveAspect = true;
                break;
            default:
                points.sprite = starsImages_SO.starImages.star_0;
                points.preserveAspect = true;
           
                break;
        }

    }

    public void SetCounterMoneyPositions()
    {

        RectTransform rectTransform = counter.GetComponent<RectTransform>();
        
        Vector2 moneyPosition1 = new Vector2(customerManager.customerSprite.position.x,customerManager.customerSprite.position.y- 2.5f);
        Vector2 moneyPosition2 = new Vector2(customerManager.customerSprite.position.x + 1f, customerManager.customerSprite.position.y - 2.5f);
        Vector2 moneyPosition3 = new Vector2(customerManager.customerSprite.position.x + 2f, customerManager.customerSprite.position.y - 2.5f);
        Vector2 moneyPosition4 = new Vector2(customerManager.customerSprite.position.x, customerManager.customerSprite.position.y - 3.5f);
        Vector2 moneyPosition5 = new Vector2(customerManager.customerSprite.position.x + 1f, customerManager.customerSprite.position.y - 3.5f);
        Vector2 moneyPosition6 = new Vector2(customerManager.customerSprite.position.x + 2f, customerManager.customerSprite.position.y - 3.5f);
        Vector2 moneyPosition7 = new Vector2(customerManager.customerSprite.position.x, customerManager.customerSprite.position.y - 4.5f);
        Vector2 moneyPosition8 = new Vector2(customerManager.customerSprite.position.x + 1f, customerManager.customerSprite.position.y - 4.5f); 
        Vector2 moneyPosition9 = new Vector2(customerManager.customerSprite.position.x + 2f, customerManager.customerSprite.position.y - 4.5f);
        Vector2 moneyPosition10 = new Vector2(customerManager.customerSprite.position.x + 1f, customerManager.customerSprite.position.y - 5.5f);


        moneyPositions.Add(moneyPosition1);
        moneyPositions.Add(moneyPosition2);
        moneyPositions.Add(moneyPosition3);
        moneyPositions.Add(moneyPosition4);
        moneyPositions.Add(moneyPosition5);
        moneyPositions.Add(moneyPosition6);
        moneyPositions.Add(moneyPosition7);
        moneyPositions.Add(moneyPosition8);
        moneyPositions.Add(moneyPosition9);
        moneyPositions.Add(moneyPosition10);
    }

}
