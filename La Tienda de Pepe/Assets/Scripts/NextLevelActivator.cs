﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NextLevelActivator : MonoBehaviour
{

    public GameObject nextLevelButton;

    private LevelManager lm;
    private Button button;
    public int currentLevel;
    public int nextLevel;
    
    // Start is called before the first frame update


    void Start()
    {
        lm = GetComponent<LevelManager>();
        button = nextLevelButton.GetComponent<Button>();
        button.onClick.AddListener(lm.gameManager.soundManager.PlaySelectLevel);
    }

    // Update is called once per frame
    void Update()
    {

        currentLevel = lm.levelName;

        switch (currentLevel) //Se utiliza en escena de juegos level1
        {
            case 8:
                if (lm.gameManager.saveSystem.level8Points != -99)
                {
                    nextLevel = 1;
                    nextLevelButton.SetActive(true);
                }
                else
                {
                    nextLevelButton.SetActive(false);
                }
                
                break;
            case 1:

                if (lm.gameManager.saveSystem.level1Points != -99)
                {
                    nextLevel = 2;
                    nextLevelButton.SetActive(true);
                }
                else
                {
                    nextLevelButton.SetActive(false);
                }
                break;
            case 2:
                if (lm.gameManager.saveSystem.level2Points != -99)
                {
                    nextLevel = 3;
                    nextLevelButton.SetActive(true);
                }
                else
                {
                    nextLevelButton.SetActive(false); ;
                }
                break;
            case 3:
                if (lm.gameManager.saveSystem.level3Points != -99)
                {
                    nextLevel = 4;
                    nextLevelButton.SetActive(true);
                }
                else
                {
                    nextLevelButton.SetActive(false);
                }
                break;
            case 4:
                if (lm.gameManager.saveSystem.level4Points != -99)
                {
                    nextLevel = 5;
                    nextLevelButton.SetActive(true);
                }
                else
                {
                    nextLevelButton.SetActive(false);
                }
                break;
            case 5:
                if (lm.gameManager.saveSystem.level5Points != -99)
                {
                    nextLevel = 6;
                    nextLevelButton.SetActive(true);
                }
                else
                {
                    nextLevelButton.SetActive(false);
                }
                break;
            case 6:
                if (lm.gameManager.saveSystem.level6Points != -99)
                {
                    nextLevel = 7;
                    nextLevelButton.SetActive(true);
                }
                else
                {
                    nextLevelButton.SetActive(false);
                }
                break;
            case 7:
                if (lm.gameManager.saveSystem.level7Points != -99)
                {
                    nextLevel = 8;
                    nextLevelButton.SetActive(true);
                }
                else
                {
                    nextLevelButton.SetActive(false);
                }
                break;
        }
    }

    public void GoToNextLevel() { //se utiliza en escena donde se escogen los niveles
        switch (nextLevel)
        {
            case 8:
                lm.gameManager.ToLevels();
                break;
            case 1:
                lm.gameManager.ToLevel1();
                break;
            case 2:
                lm.gameManager.ToLevel2();
                break;
            case 3:
                lm.gameManager.ToLevel3();

                break;
            case 4:
                lm.gameManager.ToLevel4();

                break;
            case 5:
                lm.gameManager.ToLevel5();

                break;
            case 6:
                lm.gameManager.ToLevel6();

                break;
            case 7:
                lm.gameManager.ToLevel7();

                break;
        }

        lm.gameManager.soundManager.PlayUIButton();
    }


}
