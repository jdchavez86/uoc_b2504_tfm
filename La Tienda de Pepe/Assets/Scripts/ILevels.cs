﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILevels 
{
    void SetRoundMount(); // Método que establece el monto de la ronda.
    void CalculateObjectsQuantity();
    void SetObjectsQuantity();
    void SearchAndSetObjects();
    void ShowObjects();
    void SetAndShowMoney();
    void ShowQuestion();
    void CheckInputAnwser();
    void CheckObjectAnwser();
    void GiveFeedback(int x);
    void ResolveTrial(int x);
    void SetClientMoney();
    void Pay();
    void RestartTrial();
    void CheckLevelSuccess();
    void ChangeLevel();
    void GiveStars();
    void Save();
    void UpdateState();
    void OnTriggerEnter(Collider2D col);
    void OnTriggerExit(Collider2D col);
}
