﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu (fileName ="CashData", menuName = "Cash Data", order =1)]
public class Cash_SO : ScriptableObject
{
    public CoinClass coinClass;
    public BillClass billClass;
}

[System.Serializable]
public class CoinClass {

    public OneCoinClass oneCoinclass;
    public TwoCoinClass twoCoinClass;
    public FiveCoinClass fiveCoinClass;
    
    
    public CoinClass(OneCoinClass new_oneCoinClass,TwoCoinClass new_twoCoinClass,FiveCoinClass new_fiveCoinClass)
    {
        oneCoinclass = new_oneCoinClass;
        twoCoinClass = new_twoCoinClass;
        fiveCoinClass = new_fiveCoinClass;
    }
}
[System.Serializable]
public class BillClass
{

    public TenBillClass tenBillClass;
    public TwentyBillClass twentyBillClass;
    public FiftyBillClass fiftyBillClass;
    

    public BillClass(TenBillClass new_tenBillClass, TwentyBillClass new_twentyBillClass, FiftyBillClass new_fiftyBillClass)
    {
        tenBillClass = new_tenBillClass;
        twentyBillClass = new_twentyBillClass;
        fiftyBillClass = new_fiftyBillClass;
    }
}


[System.Serializable]
public class OneCoinClass
{
    public GameObject money;
    public string coinName;
    public int amount;
    public int price;
    public Sprite coinSprite;
}

[System.Serializable]
public class TwoCoinClass
{
    public GameObject money;
    public string coinName;
    public int amount;
    public int price;
    public Sprite coinSprite;
}

[System.Serializable]
public class FiveCoinClass
{
    public GameObject money;
    public string coinName;
    public int amount;
    public int price;
    public Sprite coinSprite;
}

[System.Serializable]
public class TenBillClass
{
    public GameObject money;
    public string billName;
    public int amount;
    public int price;
    public Sprite billSprite;
}

[System.Serializable]
public class TwentyBillClass
{
    public GameObject money;
    public string billName;
    public int amount;
    public int price;
    public Sprite billSprite;
}

[System.Serializable]
public class FiftyBillClass
{
    public GameObject money;    
    public string billName;
    public int amount;
    public int price;
    public Sprite billSprite;
}