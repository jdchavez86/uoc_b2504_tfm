﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class TutorialManager : MonoBehaviour
{

    private Image imageComponent;
    private SoundManager soundManger;
    public TextMeshProUGUI helpText;
    public TextMeshProUGUI tutorialPageIndicator;
    public Button buttonPrev;
    public Button buttonNext;
    public Sprite[] tutorialImages;
    public string[] tutorialText;
    private int imageCounter;
    [SerializeField]
    private GameObject buttonDesactive;
    private GameObject canvas;
    private Canvas canvasHelpC;

    // Start is called before the first frame update
    void Start()
    {
        soundManger = FindObjectOfType<SoundManager>();
        imageComponent = GetComponent<Image>();
        imageComponent.sprite = tutorialImages[0];
        helpText.text = tutorialText[0];
        imageComponent.preserveAspect = true;
        canvas = GameObject.Find("Canvas_help");
        canvasHelpC = canvas.GetComponent<Canvas>();
        ButtonActivator();
        ChangePage();
    }

    // Update is called once per frame
    void Update()
    {
        // buttonNext.enabled = true;
        if (GameManager.help == false)
        {
            imageCounter = 0;
            imageComponent.sprite = tutorialImages[0];
            helpText.text = tutorialText[0];
            tutorialPageIndicator.text = 1 + "/6";
        }
    }

    public void NextImage()
    {
        if (imageCounter < tutorialImages.Length - 1)
        {
            imageCounter++;
            imageComponent.sprite = tutorialImages[imageCounter];
            helpText.text = tutorialText[imageCounter];
            // Debug.Log("next image");
        }
        ButtonActivator();
        ChangePage();
        soundManger.PlayUIButton();


    }

    public void PrevImage()
    {
        if (imageCounter > 0)
        {
            imageCounter--;
            imageComponent.sprite = tutorialImages[imageCounter];
            helpText.text = tutorialText[imageCounter];
        }
        ButtonActivator();
        ChangePage();
        soundManger.PlayUIButton();
    }

    public void ButtonActivator()
    {
        if (imageCounter > 0)
        {
            buttonPrev.enabled = true;
            Color tempColor = buttonPrev.GetComponent<Image>().color;
            tempColor.a = 1f;
            buttonPrev.GetComponent<Image>().color = tempColor;
        }
        else
        {
            buttonPrev.enabled = false;
            Color tempColor = buttonPrev.GetComponent<Image>().color;
            tempColor.a = 0f;
            buttonPrev.GetComponent<Image>().color = tempColor;

            GameObject eventsystem = GameObject.Find("EventSystem");
            eventsystem.GetComponent<EventSystem>().SetSelectedGameObject(buttonNext.gameObject);
        }

        if (imageCounter < tutorialImages.Length - 1)
        {
            buttonNext.enabled = true;
            ActiveDesactiveButton(false);
            Color tempColor = buttonNext.GetComponent<Image>().color;
            tempColor.a = 1f;
            buttonNext.GetComponent<Image>().color = tempColor;

        }
        else
        {
            buttonNext.enabled = false;
            ActiveDesactiveButton(true);
            Color tempColor = buttonNext.GetComponent<Image>().color;
            tempColor.a = 0f;
            buttonNext.GetComponent<Image>().color = tempColor;
            GameObject eventsystem = GameObject.Find("EventSystem");
            eventsystem.GetComponent<EventSystem>().SetSelectedGameObject(buttonPrev.gameObject);
        }
    }

    public void DesactiveCanva()
    {
        canvasHelpC.enabled = false;
    }
    void ActiveDesactiveButton(bool isActive)
    {
        if (isActive)
        {
            buttonDesactive.SetActive(true);
        }
        else { buttonDesactive.SetActive(false); }
    }

    public void ChangePage()
    {
        int tempInt = imageCounter + 1;
        string tempString = tempInt.ToString();
        tutorialPageIndicator.text = tempString + "/6";
    }

}
