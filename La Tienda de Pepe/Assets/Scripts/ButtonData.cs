﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ButtonData : MonoBehaviour
{
    public ObjectDetector objectDetector;
    public PriceOutput priceOutput;
    private GameManager gameManager;
    [HideInInspector] string buttonNumber;

    
    // Start is called before the first frame update
    void Start()
    {
        buttonNumber = GetComponent<Text>().text;
        gameManager = FindObjectOfType<GameManager>();
    }
        
    // Update is called once per frame
    void Update()
    {

    }

    public void SendNumber()
    {
        if (objectDetector.selectedProducts.Count > 0)
        {
            priceOutput.buttonNumber = buttonNumber;
            priceOutput.AddNewNumber();
        }

        gameManager.soundManager.PlayKey();
    }
}
