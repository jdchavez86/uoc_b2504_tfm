﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsColliderResizer : MonoBehaviour
{
    GameObject[] products;
    private int resolutionX;
    private int resolutionY;
    private float width;
    private float height;

    private void Awake()
    {
        resolutionX = Screen.width;
        resolutionY = Screen.height;
    }


    // Start is called before the first frame update
    void Start()
    {
        products = GameObject.FindGameObjectsWithTag("product");
        ResizeCollider();
    }

    // Update is called once per frame
    void Update()
    {
        if (resolutionX != Screen.width || resolutionY != Screen.height) // Con esto puedo detectar cambios en la resolución.
        {

            resolutionX = Screen.width;
            resolutionY = Screen.height;
            ResizeCollider();

        }
    }

    void ResizeCollider()
    {
        foreach(GameObject product in products)
        {

            if (product != null){

                RectTransform rectTransform = product.GetComponent<RectTransform>();
                SpriteRenderer spriteRenderer = product.GetComponent<SpriteRenderer>();
                BoxCollider2D boxCollider2D = product.GetComponent<BoxCollider2D>();
                rectTransform.sizeDelta = new Vector2(spriteRenderer.size.x, spriteRenderer.size.y);
                boxCollider2D.size = new Vector2(spriteRenderer.size.x, spriteRenderer.size.y);
            }
        }
    }
}
