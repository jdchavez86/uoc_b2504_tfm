﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu (fileName ="ProductsData", menuName = "Products Data", order =1)]
public class Products_SO : ScriptableObject
{
    public FruitsClass fruits;
    public PersonalCareProductsClass personalProducts;
    public StationaryClass stationaryClass;
    public ClothesClass clothesClass;
}

// clase frutas
[System.Serializable]
public class FruitsClass {

    public AppleClass appleClass;
    public PearClass pearClass;
    public LemonClass lemonClass;
    public BananaClass bananaClass;

    public FruitsClass(AppleClass new_appleClass,PearClass new_pearClass,LemonClass new_lemonClass,BananaClass new_bananaClass)
    {
        appleClass = new_appleClass;
        pearClass = new_pearClass;
        lemonClass = new_lemonClass;
        bananaClass = new_bananaClass;
    }
}

[System.Serializable]
public class AppleClass
{
    public GameObject product;
    public string fruitName;
    public int productId;
    public int amount;
    public int price;
    public Sprite appleSprite;
    public Sprite appleSpriteAlternative;
}

[System.Serializable]
public class PearClass
{
    public GameObject product;
    public string fruitName;
    public int productId;
    public int amount;
    public int price;
    public Sprite pearSprite;
    public Sprite alternativeSprite;
}

[System.Serializable]
public class LemonClass
{
    public GameObject product;
    public int productId;
    public string fruitName;
    public int amount;
    public int price;
    public Sprite lemonSprite;
    public Sprite alternativeSprite;
}

[System.Serializable]
public class BananaClass
{
    public GameObject product;
    public string fruitName;
    public int productId;
    public int amount;
    public int price;
    public Sprite bananaSprite;
    public Sprite alternativeSprite;
}

// clases productos de aseo

[System.Serializable]
public class PersonalCareProductsClass
{

    public SoapClass soapClass;
    public ToothbrushClass toothbrushClass;
    public ToothpasteClass toothpasteClass;
    public ShampooClass shampooClass;

    public PersonalCareProductsClass(SoapClass new_soapClass, ToothbrushClass new_toothbrushClass, ToothpasteClass new_toothpasteClass, ShampooClass new_shampoo_Class)
    {
        soapClass = new_soapClass;
        toothbrushClass = new_toothbrushClass;
        toothpasteClass = new_toothpasteClass;
        shampooClass = new_shampoo_Class;
    }
}

[System.Serializable]
public class SoapClass
{
    public GameObject product;
    public string productName;
    public int productId;
    public int amount;
    public int price;
    public Sprite productSprite;
    public Sprite alternativeSprite;

}

[System.Serializable]
public class ToothbrushClass
{
    public GameObject product;
    public string productName;
    public int productId;
    public int amount;
    public int price;
    public Sprite productSprite;
    public Sprite alternativeSprite;

}

[System.Serializable]
public class ToothpasteClass
{
    public GameObject product;
    public string productName;
    public int productId;
    public int amount;
    public int price;
    public Sprite productSprite;
    public Sprite alternativeSprite;

}

[System.Serializable]
public class ShampooClass
{
    public GameObject product;
    public int productId;
    public string productName;
    public int amount;
    public int price;
    public Sprite productSprite;
    public Sprite alternativeSprite;

}

// Clase productos de papelería

[System.Serializable]
public class StationaryClass
{

    public RulerClass rulerClass;
    public PencilClass pencilClass;
    public NotebookClass notebookClass;
    public ScissorsClass scissorsClass;

    public StationaryClass(RulerClass new_rulerClass, PencilClass new_pencilClass, NotebookClass new_notebookClass, ScissorsClass new_scissorsClass)
    {
        rulerClass = new_rulerClass;
        pencilClass = new_pencilClass;
        notebookClass = new_notebookClass;
        scissorsClass = new_scissorsClass;
    }
}

[System.Serializable]
public class RulerClass
{
    public GameObject product;
    public int productId;
    public string productName;
    public int amount;
    public int price;
    public Sprite productSprite;
    public Sprite alternativeSprite;
}

[System.Serializable]
public class PencilClass
{
    public GameObject product;
    public int productId;
    public string productName;
    public int amount;
    public int price;
    public Sprite productSprite;
    public Sprite alternativeSprite;
}

[System.Serializable]
public class NotebookClass
{
    public GameObject product;
    public int productId;
    public string productName;
    public int amount;
    public int price;
    public Sprite productSprite;
    public Sprite alternativeSprite;
}

[System.Serializable]
public class ScissorsClass
{
    public GameObject product;
    public int productId;
    public string productName;
    public int amount;
    public int price;
    public Sprite productSprite;
    public Sprite alternativeSprite;
}


//clases ropa

[System.Serializable]
public class ClothesClass
{

    public JeanClass jeanClass;
    public ShirtClass shirtClass;
    public SkirtClass skirtClass;
    public ShoesClass shoesClass;

    public ClothesClass(JeanClass new_jeanClass, ShirtClass new_shirtClass, SkirtClass new_skirtClass, ShoesClass new_shoesClass)
    {
        jeanClass = new_jeanClass;
        shirtClass = new_shirtClass;
        skirtClass = new_skirtClass;
        shoesClass = new_shoesClass;
    }
}

[System.Serializable]
public class JeanClass
{
    public GameObject product;
    public int productId;
    public string productName;
    public int amount;
    public int price;
    public Sprite productSprite;
    public Sprite alternativeSprite;
}

[System.Serializable]
public class ShirtClass
{
    public GameObject product;
    public int productId;
    public string productName;
    public int amount;
    public int price;
    public Sprite productSprite;
    public Sprite alternativeSprite;
}

[System.Serializable]
public class SkirtClass
{
    public GameObject product;
    public int productId;
    public string productName;
    public int amount;
    public int price;
    public Sprite productSprite;
    public Sprite alternativeSprite;
}

[System.Serializable]
public class ShoesClass
{
    public GameObject product;
    public int productId;
    public string productName;
    public int amount;
    public int price;
    public Sprite productSprite;
    public Sprite alternativeSprite;
}