﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Money : MonoBehaviour
{
    public Cash_SO cash_SO;
    public string moneyName;
    public int value;
    private string moneyTag;
    private SpriteRenderer spriteRenderer;
    private Image image;
    // Start is called before the first frame update

    void Start()
    {
        moneyTag = gameObject.tag;
        image = GetComponent<Image>();

        switch (moneyTag)
        {
            case "one_coin":

                moneyName = cash_SO.coinClass.oneCoinclass.coinName;
                value = cash_SO.coinClass.oneCoinclass.price;
                Sprite moneySprite = cash_SO.coinClass.oneCoinclass.coinSprite;
                image.sprite = moneySprite;
                image.preserveAspect = true;
                break;

            case "two_coin":

                moneyName = cash_SO.coinClass.twoCoinClass.coinName;
                value = cash_SO.coinClass.twoCoinClass.price;
                moneySprite = cash_SO.coinClass.twoCoinClass.coinSprite;
                image.sprite = moneySprite;
                image.preserveAspect = true;
                break;

            case "five_coin":

                moneyName = cash_SO.coinClass.fiveCoinClass.coinName;
                value = cash_SO.coinClass.fiveCoinClass.price;
                moneySprite = cash_SO.coinClass.fiveCoinClass.coinSprite;
                image.sprite = moneySprite;
                image.preserveAspect = true;
                break;

            case "ten_bill":

                moneyName = cash_SO.billClass.tenBillClass.billName;
                value = cash_SO.billClass.tenBillClass.price;
                moneySprite = cash_SO.billClass.tenBillClass.billSprite;
                image.sprite = moneySprite;
                image.preserveAspect = true;
                break;

            case "twenty_bill":

                moneyName = cash_SO.billClass.twentyBillClass.billName;
                value = cash_SO.billClass.twentyBillClass.price;
                moneySprite = cash_SO.billClass.twentyBillClass.billSprite;
                image.sprite = moneySprite;
                image.preserveAspect = true;
                break;

            case "fifty_bill":

                moneyName = cash_SO.billClass.fiftyBillClass.billName;
                value = cash_SO.billClass.fiftyBillClass.price;
                moneySprite = cash_SO.billClass.fiftyBillClass.billSprite;
                image.sprite = moneySprite;
                image.preserveAspect = true;
                break;
        }
    }

    void Update()
    {

    }
}