﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{

    public GameObject menu;
    public GameObject helpMenu;
    public GameObject creditsPanel;
    private GameObject buttonStartGame;
    private GameObject creditsButton;
    private GameObject buttonBackMain;
    private GameObject buttonLevels;
    private GameObject buttonRestart;
    private GameObject buttonNextLevel;
    private GameObject buttonHelp;
    private GameObject buttonLevelMenu;
    private GameObject buttonLevel1;
    private GameObject buttonNextPage;
    [HideInInspector] public EventSystem eventSystem;
    public SaveSystem saveSystem;
    public SoundManager soundManager;
    public TextAsset datafile;
    public TextAsset levelItemList;
    public static int choosedLevel;
    public static bool help = false;
    public static bool needATutorial;
    public static bool levelEnd;
    public static bool isSomeMenuActive;
    public bool isCreditsActive;
    public bool inGameScene;
    public bool inLevelsScene;

    // Start is called before the first frame update
    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        saveSystem = GetComponent<SaveSystem>(); //así funciona para prueba de niveles específicos, antes estaba en start.
    }

    void Start()
    {
        soundManager = FindObjectOfType<SoundManager>();
        

        GameObject[] otherGameManager = GameObject.FindGameObjectsWithTag("game_manager");

        if (otherGameManager.Length > 1)
        {
            Destroy(otherGameManager[1].gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

        if (SceneManager.GetActiveScene().name == "Main")
        {
            HideCredits();
        }

        if (soundManager.music.isPlaying == false) // evita que la canción se reinicie si ya empezó
        {
            soundManager.PlayOpeningMusic();
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        menu = GameObject.FindGameObjectWithTag("menu"); // Así se busca el menú al cargar la escena
        eventSystem = GameObject.FindGameObjectWithTag("event_system").GetComponent<EventSystem>();

        if (SceneManager.GetActiveScene().name == "level1")
        {
            helpMenu = GameObject.FindGameObjectWithTag("help");
            GameObject buttons = GameObject.Find("buttons");
            buttonLevels = buttons.transform.Find("Button_levels").gameObject;
            buttonRestart = buttons.transform.Find("Button_restart").gameObject;
            buttonNextLevel = buttons.transform.Find("Button_next").gameObject;
            buttonHelp = buttons.transform.Find("Button_help").gameObject;
            buttonLevelMenu = GameObject.Find("Button_level menu");
            buttonNextPage = GameObject.Find("Button_next_page");

        }

        if (SceneManager.GetActiveScene().name == "Main")
        {
            buttonStartGame = GameObject.Find("button_start");
            creditsPanel = GameObject.FindGameObjectWithTag("credits");
            creditsButton = GameObject.Find("button_credits");
        }

        if (SceneManager.GetActiveScene().name == "Levels")
        {
            buttonBackMain = GameObject.Find("button_back");
            buttonLevel1 = GameObject.Find("button_1");
            inLevelsScene = true;
            
        }
        
        isSomeMenuActive = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (inLevelsScene == true && SceneManager.GetActiveScene().name == "level1")
        {
            inGameScene = true;
            inLevelsScene = false;
            soundManager.PlayInGameMusic();
        }

        if (inGameScene == true && SceneManager.GetActiveScene().name == "Levels") // asì detecto si se venìa de scena de juego para cambiar la música.
        {
            inGameScene = false;
            inLevelsScene = true;
            soundManager.PlayOpeningMusic();
        }
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {

            if (SceneManager.GetActiveScene().name == "level1" && GameManager.levelEnd == false)
            {
                ShowLevelMenu();
            }

            if(SceneManager.GetActiveScene().name == "level1" && GameManager.levelEnd == true && GameManager.help == true)
            {
                HideHelp();
            }

            if (SceneManager.GetActiveScene().name == "Levels")
            {
                toMain();
            }

            if (SceneManager.GetActiveScene().name == "Main" && isCreditsActive == true)
            {
                HideCredits();
                eventSystem.SetSelectedGameObject(creditsButton);
            }
        }

        // todo lo que sigue es para ayudar a controlar la selecciòn de botones con el teclado, si se juega desde computador
        // el siguiente es para manejar los botones del menú
        if (SceneManager.GetActiveScene().name == "level1" && menu.GetComponent<Canvas>().enabled == true
            && eventSystem.currentSelectedGameObject != null)

        {

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {

                if (eventSystem.currentSelectedGameObject.name == "Button_levels")
                {
                    eventSystem.SetSelectedGameObject(buttonHelp);
                    return;
                }

                if (eventSystem.currentSelectedGameObject.name == "Button_restart")
                {
                    eventSystem.SetSelectedGameObject(buttonLevels);
                    return;
                }

                if (buttonNextLevel.activeSelf == false &&
                    eventSystem.currentSelectedGameObject.name == "Button_help")
                {
                    eventSystem.SetSelectedGameObject(buttonRestart);
                    return;
                }

                if (buttonNextLevel.activeSelf == true &&
                    eventSystem.currentSelectedGameObject.name == "Button_next")
                {
                    eventSystem.SetSelectedGameObject(buttonRestart);
                    return;

                }

                if (buttonNextLevel.activeSelf == true &&
                    eventSystem.currentSelectedGameObject.name == "Button_help")
                {
                    eventSystem.SetSelectedGameObject(buttonNextLevel);
                    return;

                }
                
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (eventSystem.currentSelectedGameObject.name == "Button_levels")
                {
                    eventSystem.SetSelectedGameObject(buttonRestart);
                    return;
                }

                if (buttonNextLevel.activeSelf == false &&
                    eventSystem.currentSelectedGameObject.name == "Button_restart")
                {
                    eventSystem.SetSelectedGameObject(buttonHelp);
                    return;
                }

                if (eventSystem.currentSelectedGameObject.name == "Button_help")
                {
                    eventSystem.SetSelectedGameObject(buttonLevels);
                    return;
                }

                if (buttonNextLevel.activeSelf == true &&
                  eventSystem.currentSelectedGameObject.name == "Button_next")
                {
                    eventSystem.SetSelectedGameObject(buttonHelp);
                    return;
                }

                if (buttonNextLevel.activeSelf == true &&
                    eventSystem.currentSelectedGameObject.name == "Button_restart")
                {
                    eventSystem.SetSelectedGameObject(buttonNextLevel);
                    return;
                }
            }

        }
        
        // este es para encontrar el botón a preseleccionar en caso de que nada esté seleccionado
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (eventSystem.currentSelectedGameObject == null)
            {
                if (SceneManager.GetActiveScene().name == "level1")
                {
                    if (help == false && menu.GetComponent<Canvas>().enabled == false)
                    {
                        eventSystem.SetSelectedGameObject(buttonLevelMenu);
                    }

                    if (help == false && menu.GetComponent<Canvas>().enabled == true)
                    {
                        eventSystem.SetSelectedGameObject(buttonLevels);

                    }
                }

                if (SceneManager.GetActiveScene().name == "Main")
                {
                    eventSystem.SetSelectedGameObject(buttonStartGame);
                }

                if (SceneManager.GetActiveScene().name == "Levels")
                {
                    eventSystem.SetSelectedGameObject(buttonBackMain);
                }
            }
        }

        // en la escena de selección de niveles, cambia el comportamiento de las flechas arriba y abajo según los niveles pasados
        // para ayudar a navegar más fácil la pantalla
        if (Input.GetKeyDown(KeyCode.DownArrow) && SceneManager.GetActiveScene().name == "Levels" && saveSystem.level3Points == -99)
        {
            eventSystem.SetSelectedGameObject(buttonBackMain);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && SceneManager.GetActiveScene().name == "Levels" && saveSystem.level3Points == -99 
            && eventSystem.currentSelectedGameObject.name == "button_back")
        {
            eventSystem.SetSelectedGameObject(buttonLevel1);
        }
    }

    public void ShowLevelMenu()
    {
        bool canvasEnabled = menu.GetComponent<Canvas>().enabled;

        if (canvasEnabled == false && help == false)
        {
            menu.GetComponent<Canvas>().enabled = true;
            isSomeMenuActive = true;
            eventSystem.SetSelectedGameObject(buttonLevels);
        }

        
        if (canvasEnabled == true && help == false)
        {
            CloseMenu();
        }

        if (canvasEnabled == true && help == true)
        {
            HideHelp();
        }
    }

    public void CloseMenu()
    {
        menu.GetComponent<Canvas>().enabled = false;
        isSomeMenuActive = false;
        eventSystem.SetSelectedGameObject(null);
    }

    public void HideHelp()
    {
        helpMenu.GetComponent<Canvas>().enabled = false;
        help = false;
        isSomeMenuActive = false;
        eventSystem.SetSelectedGameObject(buttonLevels);
    }

    public void ShowHelp()
    {
        helpMenu.GetComponent<Canvas>().enabled = true;
        isSomeMenuActive = true;
        help = true;
        eventSystem.SetSelectedGameObject(buttonNextPage);

    }

    public void ShowCredits()
    {
        creditsPanel.SetActive(true);
        isCreditsActive = true;
    }

    public void HideCredits()
    {
        creditsPanel.SetActive(false);
        isCreditsActive = false;
    }
 
    
    public void StartGame()
    {
        SceneManager.LoadScene("Levels");
    }

    public void toMain()
    {
        SceneManager.LoadScene("Main");
    }

    public void ToLevels()
    {
        SceneManager.LoadScene("Levels");
    }

    public void ToAnyLevel()
    {
        SceneManager.LoadScene("level1");
    }

    public void ToLevel1()
    {
        choosedLevel = 1;
        SceneManager.LoadScene("level1");
    }

    public void ToLevel2()
    {
        choosedLevel = 2;
        SceneManager.LoadScene("level1");
    }

    public void ToLevel3()
    {
        choosedLevel = 3;
        SceneManager.LoadScene("level1");
    }

    public void ToLevel4()
    {
        choosedLevel = 4;
        SceneManager.LoadScene("level1");
    }

    public void ToLevel5()
    {
        choosedLevel = 5;
        SceneManager.LoadScene("level1");
    }

    public void ToLevel6()
    {
        choosedLevel = 6;
        SceneManager.LoadScene("level1");
    }

    public void ToLevel7()
    {
        choosedLevel = 7;
        SceneManager.LoadScene("level1");
    }

    public void ToLevel0() // el nivel 0 es el caso 8 en el level manager
    {
        choosedLevel = 8;
        SceneManager.LoadScene("level1");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
