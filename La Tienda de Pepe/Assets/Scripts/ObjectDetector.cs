﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ObjectDetector : MonoBehaviour
{
    public LevelManager lm;
    private GameObject productToCustomer;
    public List<GameObject> productsOfCustomer = new List<GameObject>();
    [HideInInspector] public List<string> selectedProducts = new List<string>();
    [HideInInspector] public bool control; // variable que indica si objetos seleccionados son los mismos solicitados por cliente
    [HideInInspector] public bool giveProducts; // activa la entrega de productos al cliente
    [HideInInspector] public bool completedList = false; // sirve como bandera para armar la lista de objetos que serán entregados al cliente
    [HideInInspector] public bool returnProducts; // variable para retornar los productos al inventario en caso de respuesta incorrecta
    private bool detectObjectsEntry;
    private bool detectObjectsExit;
    public float speed = 2f; // velocidad de entrega de productos a clientes
    public float minDistance = .01f;
    public float timeToDestroy = 2f;


    void Start()
    {
        giveProducts = false;
    }

    void Update()
    {
        if (giveProducts == true) 
        {
            GiveProductsToCustomer();
        }

        if (returnProducts == true)
        {
            ReturnProductsToInventory();
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "product")
        {
            selectedProducts.Add(collision.gameObject.GetComponent<Product>().productName);

        }

        if (collision.gameObject.tag == "product" && !productsOfCustomer.Contains(collision.gameObject))
        {
            productsOfCustomer.Add(collision.gameObject);
        }

        CheckObjects();

        if (collision.gameObject.tag == "product")
        {
            //solamente opera mientras cliente está en tienda, de lo contrario, se detectarán múltiples colisiones al final y el ejercicio acaba en muchos menos intentos
            if (lm.customerManager.goIn == true)
            {
                if (selectedProducts.Count >= lm.productsToShow.Count) // confirma el input si se ha introducico la cantidad correcta de objetos o más
                {
                    lm.Confirm();
                }
            }
        }

        if (collision.gameObject.tag == "product" && CheckObjects() == true)
        {
            lm.CancelInvoke();
            lm.Confirm();
        }
     

        //lm.actualLevel.OnTriggerEnter(collision); //----> Esto no se está usando
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        
        if (collision.gameObject.tag == "product")
        {
            lm.priceOutput.text = "";
            for (int i = 0; i < selectedProducts.Count; i++)
            {
                string productName = collision.gameObject.GetComponent<Product>().productName;
                if (productName == selectedProducts[i])
                {
                    selectedProducts.RemoveAt(i);
                    i = selectedProducts.Count;
                }
            }

            // mientras la lista no esté completa, se puede retirar objetos de la misma... es verdadero cuando se entrega dinero correcto... 
            // ...o mejor dicho, cuando se confirma respuesta con la selección adecuada de objetos
            //entoncces elementos no salen de la lista y se van con el cliente o al inventario, según sea el caso... 
            // si la lista está completa, no se le hacen cambios
            if (completedList == false) 
            {
                for (int i = 0; i < productsOfCustomer.Count; i++) 
                {
                    GameObject productOfCustomer = collision.gameObject;
                    if (productOfCustomer == productsOfCustomer[i])
                    {
                        productsOfCustomer.RemoveAt(i);
                        i = productsOfCustomer.Count;
                    }
                }
            }
            
            CheckObjects();
        }

        if (collision.gameObject.tag == "product")
        {
            if (lm.customerManager.goIn == true) //todo esto opera mientras el cliente está en la tienda.
            {
                lm.CancelInvoke();
                lm.Confirm();
            }
        }

        lm.actualLevel.OnTriggerExit(collision); // Creo que esto no lo estoy usando.
    }
  
    public bool CheckObjects()
    { 
        if (selectedProducts.Count > lm.productsToShow.Count)
        {
            control = false;
        }
        else
        {
            // la siguiente operación me devuelve True aunque los productos elegidos sean más que los solicitados por el cliente
            //Pero nunca sucede dicha circunstancia porque el if anterior se encarga de ignorar esta situación
            //dejar la siguiente instrucción con el if para evitar problemas

            control = lm.productsToShow.All(x => selectedProducts.Contains(x) && lm.productsToShow.Count(a => a == x) == selectedProducts.Count(b => b == x)); 
        }

     
        return control;
    }

    public void GiveProductsToCustomer()
    {
        foreach (GameObject product in productsOfCustomer)
        {
            productToCustomer = product;

            if (productToCustomer != null) // para que no siga intentando mover un objeto que ha sido destruido.
            {
                product.GetComponent<Product>().itsAClientProduct = true;
                productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.customerManager.customerSprite.position, Time.deltaTime * speed);
                if (Vector2.Distance(productToCustomer.transform.position, lm.customerManager.customerSprite.position) < minDistance)
                {
                    product.GetComponent<Image>().enabled = false;
                    Destroy(productToCustomer, timeToDestroy);
                }
            }
        }
    }

    public void ReturnProductsToInventory()
    {
        foreach (GameObject product in productsOfCustomer)
        {
            productToCustomer = product;
            int productId = product.GetComponent<Product>().productId;

            if (productToCustomer != null) // para que no siga intentando mover un objeto que ha sido destruido.
            {
                switch (productId)
                {
                    case 11:
                        
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.appleContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.appleContainer.transform.position, Time.deltaTime*speed);
                        }

                        break;

                    case 12:
                        
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.pearContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.pearContainer.transform.position, Time.deltaTime * speed);
                        }

                        break;

                    case 13:
                        
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.lemonContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.lemonContainer.transform.position, Time.deltaTime * speed);
                        }

                        break;

                    case 14:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.bananaContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.bananaContainer.transform.position, Time.deltaTime * speed);
                        }

                        break;

                    case 21:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.soapContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.soapContainer.transform.position, Time.deltaTime * speed);
                        }
                        break;

                    case 22:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.toothbrushContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.toothbrushContainer.transform.position, Time.deltaTime * speed);
                        }
                        break;

                    case 23:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.toothpasteContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.toothpasteContainer.transform.position, Time.deltaTime * speed);
                        }
                        break;

                    case 24:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.shampooContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.shampooContainer.transform.position, Time.deltaTime * speed);
                        }
                        break;

                    case 31:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.rulerContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.rulerContainer.transform.position, Time.deltaTime * speed);
                        }
                        break;

                    case 32:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.pencilContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.pencilContainer.transform.position, Time.deltaTime * speed);
                        }
                        break;

                    case 33:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.notebookContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.notebookContainer.transform.position, Time.deltaTime * speed);
                        }
                        break;

                    case 34:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.scissorsContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.scissorsContainer.transform.position, Time.deltaTime * speed);
                        }
                        break;

                    case 41:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.jeanContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.jeanContainer.transform.position, Time.deltaTime * speed);
                        }
                        break;

                    case 42:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.shirtContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.shirtContainer.transform.position, Time.deltaTime * speed);
                        }
                        break;

                    case 43:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.skirtContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.skirtContainer.transform.position, Time.deltaTime * speed);
                        }
                        break;

                    case 44:
                        if (Vector2.Distance(productToCustomer.transform.position, lm.itemConstructor.shoesContainer.transform.position) > minDistance)
                        {
                            productToCustomer.transform.position = Vector2.MoveTowards(productToCustomer.transform.position, lm.itemConstructor.shoesContainer.transform.position, Time.deltaTime * speed);
                        }
                        break;
                }
            }
        }
    }
}
