﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using TMPro;


[System.Serializable]
public class ItemConstructor : MonoBehaviour
{
    public TextAsset itemLevelData;

    public GameObject appleContainer;
    public GameObject pearContainer;
    public GameObject bananaContainer;
    public GameObject lemonContainer;

    public GameObject soapContainer;
    public GameObject toothbrushContainer;
    public GameObject toothpasteContainer;
    public GameObject shampooContainer;

    public GameObject rulerContainer;
    public GameObject pencilContainer;
    public GameObject notebookContainer;
    public GameObject scissorsContainer;

    public GameObject jeanContainer;
    public GameObject shirtContainer;
    public GameObject skirtContainer;
    public GameObject shoesContainer;


    public GameObject oneCoinContainer;
    public GameObject twoCoinContainer;
    public GameObject fiveCoinContainer;
    public GameObject tenBillContainer;
    public GameObject twentyBillContainer;
    public GameObject fiftyBillContainer;

    public GameObject appleText;
    public GameObject pearText;
    public GameObject lemonText;
    public GameObject bananaText;
    public GameObject soapText;
    public GameObject toothbrushText;
    public GameObject toothpasteText;
    public GameObject shampooText;
    public GameObject rulerText;
    public GameObject pencilText;
    public GameObject notebookText;
    public GameObject scissorsText;
    public GameObject jeanText;
    public GameObject shirtText;
    public GameObject skirtText;
    public GameObject shoesText;

    public LevelManager lm;

    public Products_SO productSO; //SO por scriptable game object.
    public Cash_SO cashSO;

    public int level;

    
    //Fruits quantity

    public int appleQuantity;
    public int pearQuantity;
    public int bananaQuantity;
    public int lemonQuantity;

    // Personal care products quantity

    public int soapQuantity;
    public int toothbrushQuantity;
    public int toothpasteQuantity;
    public int shampooQuantity;

    // stationary products quantity

    public int rulerQuantity;
    public int pencilQuantity;
    public int notebookQuantity;
    public int scissorsQuantity;

    // clothes care products quantity

    public int jeanQuantity;
    public int shirtQuantity;
    public int skirtQuantity;
    public int shoesQuantity;

    //money

    public int oneCoinQuantity;
    public int twoCoinQuantity;
    public int fiveCoinQuantity;
    public int tenBillQuantity;
    public int twentyBillQuantity;
    public int fiftyBillQuantity;

    // Start is called before the first frame update
    void Awake()
    {
    
        LoadItemData();
        
        appleText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.fruits.appleClass.price.ToString();
        pearText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.fruits.pearClass.price.ToString();
        lemonText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.fruits.lemonClass.price.ToString();
        bananaText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.fruits.bananaClass.price.ToString(); 
        soapText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.personalProducts.soapClass.price.ToString();
        toothbrushText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.personalProducts.toothbrushClass.price.ToString();
        toothpasteText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.personalProducts.toothpasteClass.price.ToString();
        shampooText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.personalProducts.shampooClass.price.ToString();
        rulerText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.stationaryClass.rulerClass.price.ToString();
        pencilText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.stationaryClass.pencilClass.price.ToString();
        notebookText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.stationaryClass.notebookClass.price.ToString();
        scissorsText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.stationaryClass.scissorsClass.price.ToString();
        jeanText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.clothesClass.jeanClass.price.ToString();
        shirtText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.clothesClass.shirtClass.price.ToString();
        skirtText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.clothesClass.skirtClass.price.ToString();
        shoesText.GetComponent<TextMeshProUGUI>().text = "$" + productSO.clothesClass.shoesClass.price.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadItemData()
    {
        level = lm.levelName;

        TextAsset json = Resources.Load<TextAsset>("levelItemsData");
        string dataInJson = json.ToString();
        ItemListWrapper itemListWrapper = JsonUtility.FromJson<ItemListWrapper>(dataInJson);

        for (int i = 0; i < itemListWrapper.levelItemList.Count; i++) // esto lo hago para activar los contenedores de productos y dinero por nivel
        {

            if (level - 1 == i)
            {

                oneCoinContainer.SetActive(itemListWrapper.levelItemList[i].oneCoin);
                twoCoinContainer.SetActive(itemListWrapper.levelItemList[i].twoCoin);
                fiveCoinContainer.SetActive(itemListWrapper.levelItemList[i].fiveCoin);
                tenBillContainer.SetActive(itemListWrapper.levelItemList[i].tenBill);
                twentyBillContainer.SetActive(itemListWrapper.levelItemList[i].twentyBill);
                fiftyBillContainer.SetActive(itemListWrapper.levelItemList[i].fiftyBill);

                appleContainer.SetActive(itemListWrapper.levelItemList[i].apple);
                pearContainer.SetActive(itemListWrapper.levelItemList[i].pear);
                bananaContainer.SetActive(itemListWrapper.levelItemList[i].banana);
                lemonContainer.SetActive(itemListWrapper.levelItemList[i].lemon);

                soapContainer.SetActive(itemListWrapper.levelItemList[i].soap);
                toothbrushContainer.SetActive(itemListWrapper.levelItemList[i].toothbrush);
                toothpasteContainer.SetActive(itemListWrapper.levelItemList[i].toothpaste);
                shampooContainer.SetActive(itemListWrapper.levelItemList[i].shampoo);

                rulerContainer.SetActive(itemListWrapper.levelItemList[i].ruler);
                pencilContainer.SetActive(itemListWrapper.levelItemList[i].pencil);
                notebookContainer.SetActive(itemListWrapper.levelItemList[i].notebook);
                scissorsContainer.SetActive(itemListWrapper.levelItemList[i].scissors);

                jeanContainer.SetActive(itemListWrapper.levelItemList[i].jean);
                shirtContainer.SetActive(itemListWrapper.levelItemList[i].shirt);
                skirtContainer.SetActive(itemListWrapper.levelItemList[i].skirt);
                shoesContainer.SetActive(itemListWrapper.levelItemList[i].shoes);

            }
        }

        SetupLevelData();
    }

    public void SetupLevelData()
    {
        //Inventory

        // Fruits

        if (appleContainer.gameObject.activeInHierarchy == true) // Con estos métodos creamos objetos según cantidad definida en scriptable object
        {
            appleQuantity = productSO.fruits.appleClass.amount;
            for (int i = 0; i < appleQuantity; i++)
            {
                GameObject item = Instantiate(productSO.fruits.appleClass.product, appleContainer.transform.position, appleContainer.transform.rotation);
                item.transform.SetParent(appleContainer.transform,false);
                //item.transform.parent = appleContainer.transform; // así lo estaba haciendo pero aparecía un error
            }
        }

        if (pearContainer.gameObject.activeInHierarchy == true)
        {
            pearQuantity = productSO.fruits.pearClass.amount;
            for (int i = 0; i < pearQuantity; i++)
            {
                GameObject item = Instantiate(productSO.fruits.pearClass.product, pearContainer.transform.position, pearContainer.transform.rotation);
                item.transform.SetParent(pearContainer.transform,false);
            }

        }

        if (bananaContainer.gameObject.activeInHierarchy == true)
        {
            bananaQuantity = productSO.fruits.bananaClass.amount;
            for (int i = 0; i < bananaQuantity; i++)
            {
                GameObject item = Instantiate(productSO.fruits.bananaClass.product, bananaContainer.transform.position, bananaContainer.transform.rotation);
                item.transform.SetParent(bananaContainer.transform,false);
            }
        }

        if (lemonContainer.gameObject.activeInHierarchy == true)
        {
            lemonQuantity = productSO.fruits.lemonClass.amount;
            for (int i = 0; i < lemonQuantity; i++)
            {
                GameObject item = Instantiate(productSO.fruits.lemonClass.product, lemonContainer.transform.position, lemonContainer.transform.rotation);
                item.transform.SetParent(lemonContainer.transform,false);
            }
        }

        // Personal Care Products

        if (soapContainer.gameObject.activeInHierarchy == true)
        {
            soapQuantity = productSO.personalProducts.soapClass.amount;
            for (int i = 0; i < soapQuantity; i++)
            {
                GameObject item = Instantiate(productSO.personalProducts.soapClass.product, soapContainer.transform.position, soapContainer.transform.rotation);
                item.transform.SetParent(soapContainer.transform,false);
            }
        }

        if (toothbrushContainer.gameObject.activeInHierarchy == true)
        {
            toothbrushQuantity = productSO.personalProducts.toothbrushClass.amount;
            for (int i = 0; i < toothbrushQuantity; i++)
            {
                GameObject item = Instantiate(productSO.personalProducts.toothbrushClass.product, toothbrushContainer.transform.position, toothbrushContainer.transform.rotation);
                item.transform.SetParent(toothbrushContainer.transform,false);
            }
        }

        if (toothpasteContainer.gameObject.activeInHierarchy == true)
        {
            toothpasteQuantity = productSO.personalProducts.toothpasteClass.amount;
            for (int i = 0; i < toothpasteQuantity; i++)
            {
                GameObject item = Instantiate(productSO.personalProducts.toothpasteClass.product, toothpasteContainer.transform.position, toothpasteContainer.transform.rotation);
                item.transform.SetParent(toothpasteContainer.transform,false);
            }
        }

        if (shampooContainer.gameObject.activeInHierarchy == true)
        {
            shampooQuantity = productSO.personalProducts.shampooClass.amount;
            for (int i = 0; i < shampooQuantity; i++)
            {
                GameObject item = Instantiate(productSO.personalProducts.shampooClass.product, shampooContainer.transform.position, shampooContainer.transform.rotation);
                item.transform.SetParent(shampooContainer.transform,false);
            }
        }

        // papelería

        if (rulerContainer.gameObject.activeInHierarchy == true) 
        {
            rulerQuantity = productSO.stationaryClass.rulerClass.amount;
            for (int i = 0; i < rulerQuantity; i++)
            {
                GameObject item = Instantiate(productSO.stationaryClass.rulerClass.product, rulerContainer.transform.position, rulerContainer.transform.rotation);
                item.transform.SetParent(rulerContainer.transform,false);
                
            }
        }

        if (pencilContainer.gameObject.activeInHierarchy == true)
        {
            pencilQuantity= productSO.stationaryClass.pencilClass.amount;
            for (int i = 0; i < pencilQuantity; i++)
            {
                GameObject item = Instantiate(productSO.stationaryClass.pencilClass.product, pencilContainer.transform.position, pencilContainer.transform.rotation);
                item.transform.SetParent(pencilContainer.transform, false);

            }
        }

        if (notebookContainer.gameObject.activeInHierarchy == true)
        {
            notebookQuantity = productSO.stationaryClass.notebookClass.amount;
            for (int i = 0; i < notebookQuantity; i++)
            {
                GameObject item = Instantiate(productSO.stationaryClass.notebookClass.product, notebookContainer.transform.position, notebookContainer.transform.rotation);
                item.transform.SetParent(notebookContainer.transform,false);

            }
        }

        if (scissorsContainer.gameObject.activeInHierarchy == true)
        {
            scissorsQuantity= productSO.stationaryClass.scissorsClass.amount;
            for (int i = 0; i < scissorsQuantity; i++)
            {
                GameObject item = Instantiate(productSO.stationaryClass.scissorsClass.product, scissorsContainer.transform.position, scissorsContainer.transform.rotation);
                item.transform.SetParent(scissorsContainer.transform,false);

            }
        }

        // ropa

        if (jeanContainer.gameObject.activeInHierarchy == true)
        {
            jeanQuantity = productSO.clothesClass.jeanClass.amount;
            for (int i = 0; i < jeanQuantity; i++)
            {
                GameObject item = Instantiate(productSO.clothesClass.jeanClass.product, jeanContainer.transform.position, jeanContainer.transform.rotation);
                item.transform.SetParent(jeanContainer.transform,false);

            }
        }

        if (shirtContainer.gameObject.activeInHierarchy == true)
        {
            shirtQuantity = productSO.clothesClass.shirtClass.amount;
            for (int i = 0; i < shirtQuantity; i++)
            {
                GameObject item = Instantiate(productSO.clothesClass.shirtClass.product, shirtContainer.transform.position, shirtContainer.transform.rotation);
                item.transform.SetParent(shirtContainer.transform,false);

            }
        }

        if (skirtContainer.gameObject.activeInHierarchy == true)
        {
            skirtQuantity = productSO.clothesClass.skirtClass.amount;
            for (int i = 0; i < skirtQuantity; i++)
            {
                GameObject item = Instantiate(productSO.clothesClass.skirtClass.product, skirtContainer.transform.position, skirtContainer.transform.rotation);
                item.transform.SetParent(skirtContainer.transform,false);

            }
        }

        if (shoesContainer.gameObject.activeInHierarchy == true)
        {
            shoesQuantity= productSO.clothesClass.shoesClass.amount;
            for (int i = 0; i < shoesQuantity; i++)
            {
                GameObject item = Instantiate(productSO.clothesClass.shoesClass.product, shoesContainer.transform.position, shoesContainer.transform.rotation);
                item.transform.SetParent(shoesContainer.transform,false);

            }
        }


        // Money


        if (oneCoinContainer.gameObject.activeInHierarchy == true)
        {
            oneCoinQuantity = cashSO.coinClass.oneCoinclass.amount;

            for (int i = 0; i < oneCoinQuantity; i++)
            {
                GameObject item = Instantiate(cashSO.coinClass.oneCoinclass.money, oneCoinContainer.transform.position, oneCoinContainer.transform.rotation);
                item.transform.SetParent(oneCoinContainer.transform,false);
            }
        }


        if (twoCoinContainer.gameObject.activeInHierarchy == true)
        {
            twoCoinQuantity = cashSO.coinClass.twoCoinClass.amount;

            for (int i = 0; i < twoCoinQuantity; i++)
            {
                GameObject item = Instantiate(cashSO.coinClass.twoCoinClass.money, twoCoinContainer.transform.position, twoCoinContainer.transform.rotation);
                item.transform.SetParent(twoCoinContainer.transform,false);
            }
        }

        if (fiveCoinContainer.gameObject.activeInHierarchy == true)
        {
            fiveCoinQuantity = cashSO.coinClass.fiveCoinClass.amount;

            for (int i = 0; i < fiveCoinQuantity; i++)
            {
                GameObject item = Instantiate(cashSO.coinClass.fiveCoinClass.money, fiveCoinContainer.transform.position, fiveCoinContainer.transform.rotation);
                item.transform.SetParent(fiveCoinContainer.transform,false);
            }
        }

        if (tenBillContainer.gameObject.activeInHierarchy == true)
        {
            tenBillQuantity = cashSO.billClass.tenBillClass.amount;

            for (int i = 0; i < tenBillQuantity; i++)
            {
                GameObject item = Instantiate(cashSO.billClass.tenBillClass.money, tenBillContainer.transform.position, tenBillContainer.transform.rotation);
                item.transform.SetParent(tenBillContainer.transform,false);
            }
        }


        if (twentyBillContainer.gameObject.activeInHierarchy == true)
        {
            twentyBillQuantity = cashSO.billClass.twentyBillClass.amount;

            for (int i = 0; i < twentyBillQuantity; i++)
            {
                GameObject item = Instantiate(cashSO.billClass.twentyBillClass.money, twentyBillContainer.transform.position, twentyBillContainer.transform.rotation);
                item.transform.SetParent(twentyBillContainer.transform,false);
            }
        }

        if (fiftyBillContainer.gameObject.activeInHierarchy == true)
        {
            fiftyBillQuantity = cashSO.billClass.fiftyBillClass.amount;

            for (int i = 0; i < fiftyBillQuantity; i++)
            {
                GameObject item = Instantiate(cashSO.billClass.fiftyBillClass.money, fiftyBillContainer.transform.position, fiftyBillContainer.transform.rotation);
                item.transform.SetParent(fiftyBillContainer.transform,false);
            }
        }
    }
}

[System.Serializable]
public class ItemListWrapper
{
    public List<LevelItemList> levelItemList;
    
    public ItemListWrapper(List<LevelItemList> _levelItemList)
    {
        levelItemList = _levelItemList;   
    }
}

// Esto es para saber qué items poner...
[System.Serializable]
public class LevelItemList
{
    public int level;
    public bool oneCoin;
    public bool twoCoin;
    public bool fiveCoin;
    public bool tenBill;
    public bool twentyBill;
    public bool fiftyBill;
    public bool apple;
    public bool pear;
    public bool banana;
    public bool lemon;
    public bool soap;
    public bool toothbrush;
    public bool toothpaste;
    public bool shampoo;
    public bool ruler;
    public bool pencil;
    public bool notebook;
    public bool scissors;
    public bool jean;
    public bool shirt;
    public bool skirt;
    public bool shoes;
}
