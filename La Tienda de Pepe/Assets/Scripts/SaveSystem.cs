﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using UnityEditor;
using UnityEngine.UI;

public class SaveSystem : MonoBehaviour
{

    string userName = "user1";
    string fileName = "data.json";
    int currentLevelPoints;

    [HideInInspector] public int level1Points;
    [HideInInspector] public int level2Points;
    [HideInInspector] public int level3Points;
    [HideInInspector] public int level4Points;
    [HideInInspector] public int level5Points;
    [HideInInspector] public int level6Points;
    [HideInInspector] public int level7Points;
    [HideInInspector] public int level8Points;
    [HideInInspector] public List<UserData> users = new List<UserData>(); // la lista tiene que estar creada para que pueda ser poblada con la información que se cargará.

    
    // Start is called before the first frame update
    void Start()
    {
        LoadData(); // este se llamará realmente cuando se le dé la orden, en un futuro, cuando el usuario digita su nombre.
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LoadData()
    {
        // string filePath = Application.dataPath + dataFilePath + fileName;

        string filePath = Path.Combine(Application.persistentDataPath, fileName);

        if (!File.Exists(filePath))
        {
            TextAsset json = Resources.Load<TextAsset>("data");
            string dataInJson = json.ToString();

            UsersDataWrapper dataWrapper = JsonUtility.FromJson<UsersDataWrapper>(dataInJson);
            for (int i = 0; i < dataWrapper.users.Count; i++)
            {
                if (userName == dataWrapper.users[i].userName)
                {
                    userName = dataWrapper.users[i].userName;
                    level1Points = dataWrapper.users[i].level1;
                    level2Points = dataWrapper.users[i].level2;
                    level3Points = dataWrapper.users[i].level3;
                    level4Points = dataWrapper.users[i].level4;
                    level5Points = dataWrapper.users[i].level5;
                    level6Points = dataWrapper.users[i].level6;
                    level7Points = dataWrapper.users[i].level7;
                    level8Points = dataWrapper.users[i].level8;
                }
            }
        }
        else
        {
            string dataInJson = File.ReadAllText(filePath);
            UsersDataWrapper dataWrapper = JsonUtility.FromJson<UsersDataWrapper>(dataInJson);
            for (int i = 0; i < dataWrapper.users.Count; i++)
            {
                if (userName == dataWrapper.users[i].userName)
                {
                    userName = dataWrapper.users[i].userName;
                    level1Points = dataWrapper.users[i].level1;
                    level2Points = dataWrapper.users[i].level2;
                    level3Points = dataWrapper.users[i].level3;
                    level4Points = dataWrapper.users[i].level4;
                    level5Points = dataWrapper.users[i].level5;
                    level6Points = dataWrapper.users[i].level6;
                    level7Points = dataWrapper.users[i].level7;
                    level8Points = dataWrapper.users[i].level8;
                }
            }
        }
    }

    public void SaveDataByLevel(int level, int stars)
    {
        switch (level)
        {
            case 1:
                currentLevelPoints = level1Points;
                break;
            case 2:
                currentLevelPoints = level2Points;
                break;
            case 3:
                currentLevelPoints = level3Points;
                break;
            case 4:
                currentLevelPoints = level4Points;
                break;
            case 5:
                currentLevelPoints = level5Points;
                break;
            case 6:
                currentLevelPoints = level6Points;
                break;
            case 7:
                currentLevelPoints = level7Points;
                break;
            case 8:
                currentLevelPoints = level8Points;
                break;
        }

        if (stars > currentLevelPoints)
        {
            currentLevelPoints = stars;

            switch (level)
            {
                case 1:
                    level1Points = currentLevelPoints;
                    break;
                case 2:
                    level2Points = currentLevelPoints;
                    break;
                case 3:
                    level3Points = currentLevelPoints;
                    break;
                case 4:
                    level4Points = currentLevelPoints;
                    break;
                case 5:
                    level5Points = currentLevelPoints;
                    break;
                case 6:
                    level6Points = currentLevelPoints;
                    break;
                case 7:
                    level7Points = currentLevelPoints;
                    break;
                case 8:
                    level8Points = currentLevelPoints;
                    break;
            }
        }

        if (!File.Exists(Path.Combine(Application.persistentDataPath, fileName))){

            TextAsset json = Resources.Load<TextAsset>("data");
            string dataInJson = json.ToString();
            UsersDataWrapper dataWrapper = JsonUtility.FromJson<UsersDataWrapper>(dataInJson);
            for (int i = 0; i < dataWrapper.users.Count; i++)
            {
                if (userName == dataWrapper.users[i].userName)
                {
                    switch (level)
                    {
                        case 1:
                            dataWrapper.users[i].level1 = level1Points;
                            break;
                        case 2:
                            dataWrapper.users[i].level2 = level2Points;
                            break;
                        case 3:
                            dataWrapper.users[i].level3 = level3Points;
                            break;
                        case 4:
                            dataWrapper.users[i].level4 = level4Points;
                            break;
                        case 5:
                            dataWrapper.users[i].level5 = level5Points;
                            break;
                        case 6:
                            dataWrapper.users[i].level6 = level6Points;
                            break;
                        case 7:
                            dataWrapper.users[i].level7 = level7Points;
                            break;
                        case 8:
                            dataWrapper.users[i].level8 = level8Points;
                            break;
                    }
                }
            }

            dataInJson = JsonUtility.ToJson(dataWrapper, true);
            File.WriteAllText(Path.Combine(Application.persistentDataPath,fileName), dataInJson);

        }
        else
        {
            string dataInJson = File.ReadAllText(Path.Combine(Application.persistentDataPath, fileName));
            UsersDataWrapper dataWrapper = JsonUtility.FromJson<UsersDataWrapper>(dataInJson);
            for (int i = 0; i < dataWrapper.users.Count; i++)
            {
                if (userName == dataWrapper.users[i].userName)
                {
                    switch (level)
                    {
                        case 1:
                            dataWrapper.users[i].level1 = level1Points;
                            break;
                        case 2:
                            dataWrapper.users[i].level2 = level2Points;
                            break;
                        case 3:
                            dataWrapper.users[i].level3 = level3Points;
                            break;
                        case 4:
                            dataWrapper.users[i].level4 = level4Points;
                            break;
                        case 5:
                            dataWrapper.users[i].level5 = level5Points;
                            break;
                        case 6:
                            dataWrapper.users[i].level6 = level6Points;
                            break;
                        case 7:
                            dataWrapper.users[i].level7 = level7Points;
                            break;
                        case 8:
                            dataWrapper.users[i].level8 = level8Points;
                            break;
                    }
                }
            }

            dataInJson = JsonUtility.ToJson(dataWrapper, true);
            File.WriteAllText(Path.Combine(Application.persistentDataPath, fileName), dataInJson);
        }
    }
}

[System.Serializable]
public class UsersDataWrapper
{
    public List<UserData> users;

    public UsersDataWrapper(List<UserData> _userDataList)
    {
        users = _userDataList;
    }
}

// Esto es para saber qué puntos...
[System.Serializable]
public class UserData
{
    public string userName;
    public int level1;
    public int level2;
    public int level3;
    public int level4;
    public int level5;
    public int level6;
    public int level7;
    public int level8;

    /// para crear un nuevo elemento de esta clase se necesita que entre la siguiente información, que se asigna a las variables de la clase.
    public UserData(string userName_, int level1_, int level2_, int level3_, int level4_, int level5_, int level6_, int level7_, int level8_)
    {
        userName = userName_;
        level1 = level1_;
        level2 = level2_;
        level3 = level3_;
        level4 = level4_;
        level5 = level5_;
        level5 = level6_;
        level5 = level7_;
        level5 = level8_;
    }
}
