﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CustomerPrefabs", menuName = "Customer Prefabs", order = 1)]
public class Customer_SO : ScriptableObject
{

    public Sprite[] customers;

}
