﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerManager : MonoBehaviour
{

    public Customer_SO customerSO;
    public Transform startPoint; //de donde inicia el recorrido el cliente.
    public Transform endPoint; // a donde llega el cliente a realizar el pedido.
    public Transform customerSprite; // el sprite del cliente
    public LevelManager lm;
    public Sprite happyMouth;
    public Sprite sadMouth;
    public SpriteRenderer mouth;
    private float moveTime;
    public float speed;
    private bool stop;
    [HideInInspector] public bool goIn;
    [HideInInspector] public bool isInside; // estado del cliente, sirve para saber si ya se puede iniciar script de hacer pregunta
    
    
    // Start is called before the first frame update
    void Start()
    {
        ChooseCharacter();

        goIn = true;
        stop = true;
        isInside = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (goIn == true && stop == false) // movimiento de entrada
        {
            moveTime += Time.deltaTime * speed;
            lm.confirmButton.enabled = false; // el botón de confirmar se inactiva hasta que cliente ya está adentro
            customerSprite.position = Vector2.MoveTowards(startPoint.position, endPoint.position, moveTime);
            if (Vector2.Distance(customerSprite.position, endPoint.position) < .1f){
                moveTime = 0f;
                stop = true;
                isInside = true;
                lm.clientWaitingTime = lm.clientInitialTimeToWait;
                lm.clientIsWaiting = true;
                lm.confirmButton.enabled = true; // el botón de confirmar se inactiva hasta que cliente ya está adentro
                lm.actualLevel.SetRoundMount();
                lm.SetCounterMoneyPositions();
            } 
        }

        if (goIn == false && stop == false) //movimiento de salida
        {
            moveTime += Time.deltaTime * speed;
            customerSprite.position = Vector2.MoveTowards(endPoint.position, startPoint.position, moveTime);
            if (Vector2.Distance(customerSprite.position, startPoint.position) < .1f)
            {
                moveTime = 0f;
                stop = true;
                isInside = false;
                lm.waitingTimeText.text = "";

                // solo cliente entra y se reinicial el trial si se cumplen condiciones de NO finalización del nivel.
                if (lm.okRounds < 4 && lm.numberOfFails < 5) 
                {
                    lm.actualLevel.RestartTrial();
                }
                ChooseCharacter();
            }
        } 
    }
    
    public void ChooseCharacter() {

        transform.GetComponentInChildren<SpriteRenderer>().sprite = customerSO.customers[Random.Range(0, 15)];
        mouth.sprite = null;
    }

    public void GetIn()
    {
        goIn = true;
        stop = false;
    }

    public void GetOut()
    {
        goIn = false;
        stop = false;
    }
}
